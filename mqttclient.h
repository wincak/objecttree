#pragma once

#include "component.h"

#include <QMqttTopicName>
#include <QtMqtt/QMqttClient>

class QMqttClient;
class QMqttSubscription;
class QMqttTopicFilter;

class MqttClient : public Component {
    Q_OBJECT

    Q_PROPERTY(QString hostname READ hostname WRITE setHostname NOTIFY hostnameChanged STORED true)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged STORED true)

    Q_PROPERTY(QString topic READ topic WRITE setTopic NOTIFY topicChanged)

    Q_PROPERTY(QString state READ state NOTIFY stateChanged STORED false)

public:
    MqttClient(Component* parent = nullptr);

    QString hostname() const;
    int port() const;

    QString state() const;

    QMqttSubscription* subscribe(const QMqttTopicFilter& topic, quint8 qos = 0);

    QString topic() const;

public slots:
    void setHostname(QString hostname);
    void setPort(int port);

    Q_INVOKABLE void connectToHost();
    Q_INVOKABLE void disconnectFromHost();

    void setTopic(QString topic);

signals:
    void hostnameChanged(QString hostname);
    void portChanged(int port);

    void stateChanged(QString state);

    void connected();
    void disconnected();

    void messageReceived(QString message);

    void topicChanged(QString topic);

private slots:
    void processMessage(const QByteArray& message, const QMqttTopicName& topic);
    void processStateChange(QMqttClient::ClientState state);

private:
    QMqttClient* m_client;
    QString m_topic;
};
