#ifndef PROPERTYCHANGENOTIFIER_H
#define PROPERTYCHANGENOTIFIER_H

#include <QObject>

/*
 * Note: this whole class is sort of a hack/workaround.
 * It's sole purpose is to be able to dynamically notify that a components property has changed.
 * If there was a way to connect signal as a QMetaMethod to a lambda function, it would probably
 * not be necessary at all. Or maybe we could use QSignalMapper...oh wait, it's deprecated.
 */

class PropertyChangeNotifier : public QObject
{
    Q_OBJECT
public:
    explicit PropertyChangeNotifier(QObject* parent, int propertyIndex);

    QMetaMethod notifySlotMetaMethod() const;

signals:
    void propertyChanged(int);

public slots:
    void notifyPropertyChanged();

private:
    int m_propertyIndex;
};

#endif // PROPERTYCHANGENOTIFIER_H
