#pragma once

#include <QObject>

#include <QMetaMethod>
#include <QPointer>

// TODO: take information from Sockets instead of Senders-signals/slots

class Component;
class Socket;

class Connection : public QObject {
    Q_OBJECT

public:
    explicit Connection(QObject* parent);
    explicit Connection(QObject* parent, QPointer<Socket> sender, QPointer<Socket> receiver);

    QPointer<Socket> sender() const;
    QPointer<Socket> receiver() const;

    bool isValid() const;

    int startXPos() const;
    int startYPos() const;
    int endXPos() const;
    int endYPos() const;

signals:
    void connected();
    void disconnected();

    void startXPosChanged(int startXPos);
    void startYPosChanged(int startYPos);
    void endXPosChanged(int endXPos);
    void endYPosChanged(int endYPos);

    void isValidChanged(bool isValid);

private:
    void attemptConnect();

    QString signalSignature() const;
    QString slotSignature() const;

private:
    QMetaObject::Connection m_handle;

    QPointer<Socket> m_sender;
    QPointer<Socket> m_receiver;
};
