import QtQuick 2.4
import QtQuick.Controls 2.13
import QtQuick.Dialogs 1.2

Dialog {
    id: dialog
    width: 400
    height: 400
    title: qsTr("Create connection")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    property alias slotComboBox: slotComboBox
    property alias receiverComboBox: receiverComboBox
    property alias signalComboBox: signalComboBox
    property alias senderComboBox: senderComboBox

    ComboBox {
        id: senderComboBox
        width: 250
        anchors.top: senderLabel.bottom
        anchors.topMargin: 6
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ComboBox {
        id: signalComboBox
        width: 250
        anchors.top: senderComboBox.bottom
        anchors.topMargin: 6
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Label {
        id: senderLabel
        text: qsTr("Sender")
        anchors.topMargin: 8
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Label {
        id: receiverLabel
        text: qsTr("Receiver")
        anchors.top: signalComboBox.bottom
        anchors.topMargin: 6
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ComboBox {
        id: receiverComboBox
        width: 250
        anchors.top: receiverLabel.bottom
        anchors.topMargin: 6
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ComboBox {
        id: slotComboBox
        width: 250
        anchors.top: receiverComboBox.bottom
        anchors.topMargin: 6
        anchors.horizontalCenter: parent.horizontalCenter
    }
}

/*##^##
Designer {
    D{i:1;anchors_y:31}D{i:2;anchors_y:77}D{i:3;anchors_y:8}D{i:4;anchors_y:123}D{i:5;anchors_y:146}
D{i:6;anchors_y:192}
}
##^##*/

