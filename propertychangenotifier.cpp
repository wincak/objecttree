#include "propertychangenotifier.h"

#include <QDebug>
#include <QMetaMethod>

PropertyChangeNotifier::PropertyChangeNotifier(QObject *parent, int propertyIndex)
    : QObject(parent)
    , m_propertyIndex(propertyIndex)
{

}

QMetaMethod PropertyChangeNotifier::notifySlotMetaMethod() const
{
    const QMetaObject* metaObject = this->metaObject();

    int methodCount = metaObject->methodCount();
    for (int methodIndex = 0; methodIndex < methodCount; methodIndex++) {
        QMetaMethod method = metaObject->method(methodIndex);
        if (method.methodType() != QMetaMethod::Slot)
            continue;

        if (method.methodSignature() == "notifyPropertyChanged()")
            return method;
    }

    // We totally shouldnt get here. Are the function names the same?
    Q_ASSERT(false);

    return QMetaMethod();
}

void PropertyChangeNotifier::notifyPropertyChanged()
{
    emit propertyChanged(m_propertyIndex);
}
