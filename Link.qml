import QtQuick 2.0

Item {
    id: link

    // TODO: this is probably not really right, the link shouldn't take up more space
    // than the rectangle between head and tail
    anchors.fill: parent

    property LinkSocket tail
    property LinkSocket head

    onTailChanged: { updateCanvas() }
    onHeadChanged: { updateCanvas(); }

    function updateCanvas() {
        // TODO: clear the canvas?
        if(!head)
            return false
        if(!tail)
            return false

//        console.log("Requesting canvas update...")
//        console.log("Head X:", head.x, "Y:", head.y, "Tail X:", tail.x, "Y:", tail.y)
//        console.log("Canvas height:", canvas.height, "Width:", canvas.width)
//        console.log("Parent:", parent.width, parent.height)

        canvas.requestPaint()
    }

    Canvas {
        id: canvas

        anchors.fill: parent
        antialiasing: true
        renderStrategy: Canvas.Threaded

        onPaint: {
            var ctx = getContext("2d")
            ctx.reset()
            ctx.save()
            ctx.clearRect(0, 0, width, height)

            ctx.lineWidth = 2
            ctx.strokeStyle = "#000000"
            ctx.lineJoin = "miter"

            ctx.beginPath()
            var tPos = tail.mapToItem(canvas, 0, 0)
            console.log("tail pos:", tPos)
            ctx.moveTo(tPos.x + tail.width/2, tPos.y + tail.height/2)

            var hPos = head.mapToItem(canvas, 0, 0)
            console.log("head pos:", hPos)
            ctx.lineTo(hPos.x + head.width/2, hPos.y + head.height/2)

            ctx.closePath()

            ctx.stroke()

            ctx.restore()
        }
    }
}
