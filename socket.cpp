#include "socket.h"

#include <QMetaMethod>

#include "component.h"

Socket::Socket(Component *parent, QMetaMethod metaMethod, Qt::Edge outsideEdge, int outsideEdgeOffset)
    : QObject(parent)
    , m_outsideEdge(outsideEdge)
    , m_outsideEdgeOffset(outsideEdgeOffset)
    , m_metaMethod(metaMethod)
    , m_insideXPos(0)
    , m_insideYPos(0)
{
    Q_ASSERT(parent);

    switch (metaMethod.methodType()) {
    case QMetaMethod::Signal:
        m_type = SocketType::Signal;
        break;
    case QMetaMethod::Slot:
        m_type = SocketType::Slot;
        break;
    case QMetaMethod::Method:
    case QMetaMethod::Constructor:
        qWarning("Meta method type not applicable for a socket");
        m_type = SocketType::Slot;
    }

    m_signature = metaMethod.methodSignature();

    updateOutsideXPos();
    updateOutsideYPos();

    connect(parent, &Component::xPosChanged, this, &Socket::updateOutsideXPos);
    connect(parent, &Component::yPosChanged, this, &Socket::updateOutsideYPos);
}

Socket::~Socket()
{
}

Qt::Edge Socket::outsideEdge() const
{
    return m_outsideEdge;
}

void Socket::setOutsideEdge(Qt::Edge edge)
{
    if (edge == m_outsideEdge)
        return;

    m_outsideEdge = edge;
    emit outsideEdgeChanged(m_outsideEdge);
}

int Socket::outsideEdgeOffset() const
{
    return m_outsideEdgeOffset;
}

void Socket::setOutsideEdgeOffset(int offset)
{
    if (offset == m_outsideEdgeOffset)
        return;

    m_outsideEdgeOffset = offset;
    emit outsideEdgeOffsetChanged(m_outsideEdgeOffset);
}

int Socket::outsideXPos() const
{
    return m_outsideXPos;
}

int Socket::outsideYPos() const
{
    return m_outsideYPos;
}

SocketType Socket::type() const
{
    return m_type;
}

QString Socket::signature() const
{
    return m_signature;
}

QMetaMethod Socket::metaMethod() const
{
    return m_metaMethod;
}

int Socket::insideXPos() const
{
    return m_insideXPos;
}

void Socket::setInsideXPos(int insideXPos)
{
    if (insideXPos == m_insideXPos)
        return;

    m_insideXPos = insideXPos;
    emit insideXPosChanged(m_insideXPos);
}

int Socket::insideYPos() const
{
    return m_insideYPos;
}

void Socket::setInsideYPos(int insideYPos)
{
    if (insideYPos == m_insideYPos)
        return;

    m_insideYPos = insideYPos;
    emit insideYPosChanged(m_insideYPos);
}

void Socket::updateOutsideXPos( )
{
    Component* parent = qobject_cast<Component*>(this->parent());
    if (!parent) {
        qWarning("Failed to cast parent component");
        return;
    }

    int outsideXPos = 0;
    switch (m_outsideEdge) {
    case Qt::TopEdge:
        outsideXPos = parent->xPos() + m_outsideEdgeOffset;
        break;
    case Qt::RightEdge:
        outsideXPos = parent->xPos() + parent->width();
        break;
    case Qt::BottomEdge:
        outsideXPos = parent->xPos() + parent->width() - m_outsideEdgeOffset;
        break;
    case Qt::LeftEdge:
        outsideXPos = parent->xPos();
        break;
    }
    Q_ASSERT(outsideXPos >= 0);

    if (outsideXPos == m_outsideXPos)
        return;

    m_outsideXPos = outsideXPos;
    emit outsideXPosChanged(m_outsideXPos);
}

void Socket::updateOutsideYPos()
{
    Component* parent = qobject_cast<Component*>(this->parent());
    if (!parent) {
        qWarning("Failed to cast parent component");
        return;
    }

    int outsideYPos = 0;
    switch (m_outsideEdge) {
    case Qt::TopEdge:
        outsideYPos = parent->yPos();
        break;
    case Qt::RightEdge:
        outsideYPos = parent->yPos() + m_outsideEdgeOffset;
        break;
    case Qt::BottomEdge:
        outsideYPos = parent->yPos() + parent->height();
        break;
    case Qt::LeftEdge:
        outsideYPos = parent->yPos() + parent->height() - m_outsideEdgeOffset;
        break;
    }
    Q_ASSERT(outsideYPos >= 0);

    if (outsideYPos == m_outsideYPos)
        return;

    m_outsideYPos = outsideYPos;
    emit outsideYPosChanged(m_outsideYPos);
}
