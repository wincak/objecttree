import QtQuick 2.12
import QtQuick.Controls 2.12

SchematicForm {
    MouseArea {
        id: mouseArea
        anchors.fill: parent

        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onPressed: {
            if (mouse.button === Qt.LeftButton) {
                control.deselectAllComponents();
                schematicMenu.close();
                symbolMenu.close();
            } else {
                schematicMenu.popup()
            }
        }
    }

    Repeater {
        anchors.fill: parent

        model: control.scopeComponents

        delegate: Symbol {
            model: modelData
            mouseArea.onPressed: {
                control.selectedComponent = path;
                if (mouse.button === Qt.RightButton) {
                    symbolMenu.popup();
                } else {
                    symbolMenu.close();
                    schematicMenu.close();
                }
            }
        }
    }

    Repeater {
        anchors.fill: parent

        model: control.scopeConnections

        delegate: Wire {
            model: modelData
        }
    }

    Menu {
        id: schematicMenu
        MenuItem {
            text: qsTr("Go up");
            onClicked: control.setUpperScopeAsCurrent()
            enabled: !control.currentScopeIsRoot
        }
        MenuItem {
            text: qsTr("Create connection")
            onClicked: createConnectionDialog.open()
        }
        Menu {
            id: addMenu
            title: qsTr("Add")

            Repeater {
                model: control.componentTypes
                delegate: MenuItem {
                    text: modelData
                    onClicked: createComponent(modelData)
                }
            }
        }
    }

    Menu {
        id: symbolMenu

        MenuItem {
            text: qsTr("Open")
            onClicked: control.currentScope = control.selectedComponent
            // TODO: enable opening components with no children so some can be created
            enabled: control.selectedComponentHasChildren
        }
        MenuItem {
            text: qsTr("Remove")
            onClicked: removeSelectedComponent()
        }
    }

    CreateConnectionDialog {
        id: createConnectionDialog
        scope: control.currentScope

        visible: false
    }

    function createComponent(type) {
        let xPos = mouseArea.mouseX;
        let yPos = mouseArea.mouseY;
        let scope = control.currentScope
        control.createComponent(scope, type, xPos, yPos);
    }

    function removeSelectedComponent() {
        control.removeComponent(control.selectedComponent);
    }
}
