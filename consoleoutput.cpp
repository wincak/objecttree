#include "consoleoutput.h"

#include "console.h"

ConsoleOutput::ConsoleOutput(Component* parent)
    : Component(parent)
{
}

void ConsoleOutput::printString(const QString& string)
{
    pConsole->addMessage(string);
}

void ConsoleOutput::printInteger(int integer)
{
    pConsole->addMessage(QString::number(integer));
}
