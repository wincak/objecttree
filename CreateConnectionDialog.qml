import QtQuick 2.4
import QtQuick.Dialogs 1.2

CreateConnectionDialogForm {
    property string scope: "/"

    senderComboBox.model: control.childrenModel(scope)
    senderComboBox.onCurrentTextChanged: loadSenderSignals()

    receiverComboBox.model: control.childrenModel(scope)
    receiverComboBox.onCurrentTextChanged: loadReceiverSlots()

    onAccepted: createConnection()
    onRejected: close()

    MessageDialog {
        id: errorMessage
        text: qsTr("Connection failed")
        visible: false
    }

    function loadSenderSignals() {
        const sender = senderComboBox.currentText;
        const signals = control.signalsModel(sender);
        signalComboBox.model = signals;
    }
    function loadReceiverSlots() {
        const receiver = receiverComboBox.currentText;
        const slots = control.slotsModel(receiver);
        slotComboBox.model = slots;
    }
    function createConnection() {
        const sender = senderComboBox.currentText;
        const signal = signalComboBox.currentText;
        const receiver = receiverComboBox.currentText;
        const slot = slotComboBox.currentText;

        const success = control.createConnection(sender, signal, receiver, slot);
        if (!success)
            errorMessage.open();
    }
}
