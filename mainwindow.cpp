#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QListWidgetItem>
#include <QMetaProperty>
#include <QPushButton>
#include <QQmlContext>
#include <QTreeView>
#include <QVector>
#include <QtQuickWidgets/QQuickWidget>
#include <models/componentmodel.h>
#include <models/connectionmodel.h>

#include "client.h"
#include "connectiontablemodel.h"
#include "connectiontablemodeldelegate.h"
#include "console.h"
#include "control.h"
#include "propertymodel.h"
#include "sockettablemodel.h"
#include "treemodel.h"

#include <QDebug>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);

    Client* client = new Client(this);
    m_control = new Control(client, this);
    qmlRegisterUncreatableType<Control>("org.wincak", 1, 0, "Control", "Can't be created in QML");
    m_ui->schematic->rootContext()->setContextProperty("control", QVariant::fromValue(m_control.data()));

    connect(m_control, &Control::selectedComponentChanged, this, &MainWindow::displayComponentProperties);
    connect(m_control, &Control::selectedComponentChanged, this, &MainWindow::displayComponentSockets);
    connect(m_control, &Control::selectedComponentChanged, this, &MainWindow::displayComponentSerialized);
    connect(m_control, &Control::selectedComponentChanged, this, &MainWindow::displayComponentSignals);
    connect(m_control, &Control::selectedComponentChanged, this, &MainWindow::displayComponentSlots);
    connect(m_control, &Control::currentScopeChanged, this, &MainWindow::displayScopeConnections);
    connect(m_control, &Control::currentScopeChanged, this, &MainWindow::displayScopeSerialized);

    ConnectionTableModelDelegate* delegate = new ConnectionTableModelDelegate(m_ui->connectionTable);
    m_ui->connectionTable->setItemDelegate(delegate);
    m_ui->connectionTable->setSelectionBehavior(QAbstractItemView::SelectRows);

    QPointer<TreeModel> hierarchyModel = m_control->hierarchyModel();
    m_ui->componentTree->setModel(hierarchyModel);
    connect(m_ui->componentTree, &QTreeView::clicked, this, &MainWindow::setCurrentComponent);

    connect(m_ui->connectionAdd, &QPushButton::clicked, this, &MainWindow::addConnection);
    connect(m_ui->connectionRemove, &QPushButton::clicked, this, &MainWindow::removeConnection);

    qmlRegisterType<MainWindow>("org.wincak", 1, 0, "MainWindow");
    m_ui->schematic->rootContext()->setContextProperty("mainWindow", QVariant::fromValue(this));

    m_ui->schematic->setSource(QUrl(QStringLiteral("qrc:///Schematic.qml")));

    m_ui->consoleWindow->setText(pConsole->messages().join('\n'));
    connect(pConsole, &Console::messagesChanged, this, &MainWindow::updateConsoleMessages);

    connect(m_ui->consoleInput, &QLineEdit::returnPressed, this, &MainWindow::issueConsoleCommand);

    this->tabifyDockWidget(m_ui->dockScope, m_ui->dockConsole);
    this->tabifyDockWidget(m_ui->dockScope, m_ui->dockComponent);
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::setCurrentComponent(const QModelIndex& index)
{
    if (!index.isValid())
        return;

    Component* component = static_cast<Component*>(index.internalPointer());
    if (!component)
        return;

    m_control->setSelectedComponent(component->path());
}

void MainWindow::displayComponentProperties(const QString& path)
{
    QAbstractItemModel* oldModel = m_ui->propertiesView->model();
    if (oldModel)
        oldModel->deleteLater();

    QPointer<PropertyModel> model = m_control->propertyModel(path, this);
    if (model.isNull())
        return;

    m_ui->propertiesView->setModel(model);
}

void MainWindow::displayComponentSockets(const QString& path)
{
    QAbstractItemModel* oldModel = m_ui->socketsView->model();
    if (oldModel)
        oldModel->deleteLater();

    QPointer<SocketTableModel> model = m_control->socketModel(path, this);
    if (model.isNull())
        return;

    m_ui->socketsView->setModel(model);
}

void MainWindow::displayComponentSignals(const QString& path)
{
    m_ui->signalList->clear();

    QStringList list = m_control->signalsModel(path);
    m_ui->signalList->insertItems(0, list);
}

//void MainWindow::displayComponentSignals(QPointer<Component> scope)
//{
//    m_ui->signalList->clear();

//    QVector<QMetaMethod> signalList = scope->objectSignals();

//    int row = 0;
//    for (auto i = signalList.constBegin(); i != signalList.constEnd(); ++i) {
//        QListWidgetItem* item = new QListWidgetItem();
//        item->setData(Qt::DisplayRole, i->methodSignature());
//        m_ui->signalList->insertItem(row, item);

//        row++;
//    }
//}

void MainWindow::displayComponentSlots(const QString& path)
{
    m_ui->slotList->clear();

    QStringList list = m_control->slotsModel(path);
    m_ui->slotList->insertItems(0, list);
    //    QVector<QMetaMethod> slotList = path->objectSlots();

    //    int row = 0;
    //    for (auto i = slotList.constBegin(); i != slotList.constEnd(); ++i) {
    //        QListWidgetItem* item = new QListWidgetItem();
    //        item->setData(Qt::DisplayRole, i->methodSignature());
    //        m_ui->slotList->insertItem(row, item);

    //        row++;
    //    }
}

void MainWindow::displayComponentSerialized(const QString& path)
{
    m_ui->componentSerializedBrowser->clear();

    QVariantMap map = m_control->serializedConfig(path);
    QJsonDocument doc(QJsonObject::fromVariantMap(map));

    m_ui->componentSerializedBrowser->setText(doc.toJson());
}

void MainWindow::displayScopeSerialized(const QString& path)
{
    m_ui->scopeSerializedBrowser->clear();

    QVariantMap map = m_control->serializedConfig(path);
    QJsonDocument doc(QJsonObject::fromVariantMap(map));

    m_ui->scopeSerializedBrowser->setText(doc.toJson());
}

void MainWindow::displayScopeConnections(const QString& path)
{
    QAbstractItemModel* oldModel = m_ui->connectionTable->model();
    if (oldModel)
        oldModel->deleteLater();

    QPointer<ConnectionTableModel> model = m_control->connectionModel(path, this);
    if (model.isNull())
        return;

    m_ui->connectionTable->setModel(model);
}

void MainWindow::addConnection()
{
    if (m_control->selectedComponent().isEmpty())
        return;

    ConnectionTableModel* model = qobject_cast<ConnectionTableModel*>(m_ui->connectionTable->model());
    Q_ASSERT(model);
    if (!model)
        return;

    model->insertRow(model->rowCount());
}

void MainWindow::removeConnection()
{
    if (m_control->selectedComponent().isEmpty())
        return;

    ConnectionTableModel* model = qobject_cast<ConnectionTableModel*>(m_ui->connectionTable->model());
    Q_ASSERT(model);
    if (!model)
        return;

    QItemSelectionModel* selection = m_ui->connectionTable->selectionModel();
    if (!selection->hasSelection())
        return;

    if (!selection->selectedRows().isEmpty())
        model->removeRow(selection->selectedRows().at(0).row());
}

void MainWindow::updateConsoleMessages(QStringList messages)
{
    m_ui->consoleWindow->setText(messages.join('\n'));
}

void MainWindow::issueConsoleCommand()
{
    QString command = m_ui->consoleInput->text();
    m_control->issueCommand(command);

    m_ui->consoleInput->clear();
}
