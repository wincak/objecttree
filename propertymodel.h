#ifndef PROPERTYMODEL_H
#define PROPERTYMODEL_H

#include <QAbstractTableModel>

#include <QPointer>

#include "component.h"

class PropertyModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum PropertyIndexes {
      PropertyName = 0,
      PropertyValueType = 1,
      PropertyWritable = 2,
      PropertyValue = 3,
    };

    explicit PropertyModel(QObject* parent, Component* component);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
    void mapValueUpdates();

private slots:
    void notifyPropertyValueChanged(int index);

private:
    QPointer<Component> m_component;
};

#endif // PROPERTYMODEL_H
