#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>

#include "component.h"

class TreeModel : public QAbstractItemModel {
    Q_OBJECT

public:
    TreeModel(QPointer<Component> rootItem, QObject* parent);
    ~TreeModel() override;

    QVariant data(const QModelIndex& index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    QString selectedComponent() const;

private:
    void setupModelData(Component* parent);
    Component* getItem(const QModelIndex& index) const;

    QPointer<Component> m_rootItem;
};

#endif // TREEMODEL_H
