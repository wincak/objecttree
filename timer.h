#ifndef TIMER_H
#define TIMER_H

#include "component.h"

class QTimer;

class Timer : public Component
{
    Q_OBJECT

    Q_PROPERTY(int interval READ interval WRITE setInterval STORED true)
    Q_PROPERTY(bool singleShot READ isSingleShot WRITE setSingleShot STORED true)
    Q_PROPERTY(bool active READ isActive WRITE setActive STORED false)
    Q_PROPERTY(int remainingTime READ remainingTime STORED false)

public:
    Timer(Component* parent = nullptr);

    int interval() const;
    bool isSingleShot() const;
    bool isActive() const;
    int remainingTime() const;

public slots:
    void setInterval(int msecs);
    void setSingleShot(bool singleShot);

    void setActive(bool active);
    void start(int msecs);
    void start();
    void stop();

signals:
    void timeout();

protected:
    QTimer* m_timer;
};

#endif // TIMER_H
