#include "client.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QMetaMethod>
#include <QTimer>

#include "component.h"
#include "console.h"
#include "consoleoutput.h"
#include "counter.h"
#include "filereader.h"
#include "mqttclient.h"
#include "timer.h"
#include "socket.h"

Client::Client(QObject* parent)
    : QObject(parent)
{
    m_rootComponent = setupModelData();
}

QPointer<Component> Client::component(const QString& path) const
{
    if (path.isEmpty())
        return QPointer<Component>();

    // TODO: figure out how to deal with the root component addressing
    if (path == '/')
        return m_rootComponent;

    // Absolute path begins with /<root_name>/ that needs to be removed
    Q_ASSERT(path.startsWith('/'));
    QString subPath = path;
    subPath.remove(0, 1);

    QPointer<Component> ptr = m_rootComponent->child(subPath);
    Q_ASSERT(ptr);

    return ptr;
}

QStringList Client::componentTypes() const
{
    // TODO: needs to be done dynamically when components become loadable plugins
    QStringList list;
    list << "consoleOutput";
    list << "counter";
    list << "fileReader";
    list << "mqttClient";
    list << "timer";

    return list;
}

bool Client::createComponent(const QString& scope, const QString& type, int xPos, int yPos)
{
    // TODO: consider using Component::instantiate() or moving this process to Component completely
    QPointer<Component> parent = component(scope);
    if (parent.isNull())
        return false;

    Component* component = nullptr;
    if (type == "consoleOutput") {
        component = new ConsoleOutput(parent);
    } else if (type == "counter") {
        component = new Counter(parent);
    } else if (type == "fileReader") {
        component = new FileReader(parent);
    } else if (type == "mqttClient") {
        component = new MqttClient(parent);
    } else if (type == "timer") {
        component = new Timer(parent);
    } else {
        return false;
    }

    component->setXPos(xPos);
    component->setYPos(yPos);

    bool success = parent->addChild(component);

    return success;
}

bool Client::removeComponent(const QString& path)
{
    Component* scopePtr;
    QPointer<Component> componentPtr = component(path);
    if (componentPtr)
        scopePtr = qobject_cast<Component*>(componentPtr->parent());

    if (!componentPtr || !scopePtr)
        return false;

    bool success = scopePtr->removeChild(componentPtr->objectName());

    return success;
}

bool Client::createConnection(const QString &sender, const QString &signal, const QString &receiver, const QString &slot)
{
    if (sender.isEmpty() || signal.isEmpty()
        || receiver.isEmpty() || slot.isEmpty()) {
        qWarning("Invalid connect command arguments");
        return false;
    }

    auto senderComponent = component(sender);
    if (!senderComponent) {
        qWarning("Invalid sender %s", qPrintable(sender));
        return false;
    }

    auto signalSocket = senderComponent->socket(signal);
    if (!signalSocket) {
        qWarning("Invalid signal %s", qPrintable(signal));
        return false;
    }

    auto receiverComponent = component(receiver);
    if (!receiverComponent) {
        qWarning("Invalid receiver %s", qPrintable(receiver));
        return false;
    }

    auto slotSocket = receiverComponent->socket(slot);
    if (!slotSocket) {
        qWarning("Invalid slot %s", qPrintable(slot));
        return false;
    }

    auto senderParentComponent = qobject_cast<Component*>(senderComponent->parent());
    auto receiverParentComponent = qobject_cast<Component*>(receiverComponent->parent());
    Q_ASSERT(senderParentComponent && receiverParentComponent);

    if (senderParentComponent != receiverParentComponent) {
        qWarning("Cannot connect components that are not siblings");
        return false;
    }

    bool success = senderParentComponent->createConnection(signalSocket, slotSocket);
    if (!success) {
        qWarning("Failed to connect %s with %s",
                 qPrintable(signal), qPrintable(slot));
        return false;
    }

    return true;
}

void Client::issueCommand(const QString &command)
{
    QString echo = QString("Command: %1").arg(command);
    pConsole->addMessage(echo);

    QJsonDocument doc = QJsonDocument::fromJson(command.toUtf8());
    if (doc.isEmpty()) {
        qWarning("Failed to parse command: %s", qPrintable(command));
        return;
    }

    QVariantMap map = doc.object().toVariantMap();

    QString cmd = map.value("cmd").toString();
    if (cmd.isEmpty()) {
        qWarning("No command");
        return;
    }

    if (cmd == "connect") {
        QVariant args = map.value("args");
        processConnectCommand(args);
    } else {
        pConsole->addMessage(QString("Unknown command %1").arg(cmd));
    }
}

QPointer<Component> Client::setupModelData()
{
    Component* rootItem = new Component(nullptr);

    Component* child = new Component(rootItem);
    child->setObjectName("potomek 1");
    rootItem->addChild(child);

    auto child1 = new Component(rootItem);
    rootItem->addChild(child1);
    child1->setXPos(150);
    child1->setYPos(90);
    child1->setWidth(150);
    child1->setHeight(75);
    child1->setObjectName("potomek 2");

    auto timer = new Timer(child1);
    timer->setObjectName("timer 1");
    child1->addChild(timer);
    auto counter = new Counter(child1);
    counter->setObjectName("counter");
    child1->addChild(counter);
    auto fileReader = new FileReader(child1);
    fileReader->setObjectName("file reader");
    child1->addChild(fileReader);
    auto mqttClient = new MqttClient(child1);
    mqttClient->setObjectName("MQTT Client");
    child1->addChild(mqttClient);
    auto consoleOut = new ConsoleOutput(child1);
    consoleOut->setObjectName("Console OUT");
    child1->addChild(consoleOut);

    QTimer::singleShot(0, this, [child1, timer, counter]() {
        auto sender = timer->objectSockets().value(3);
        auto receiver = counter->objectSockets().value(11);
        bool success = child1->createConnection(sender, receiver);
    });
    timer->setYPos(150);
    counter->setXPos(300);
    counter->setYPos(150);
//    child1->connectObjects(counter, counter->signal("valueChanged", "valueChanged(int)"),
//        consoleOut, consoleOut->slot("printInteger", "printInteger(int)"));
    consoleOut->setXPos(450);
    consoleOut->setYPos(200);

    return rootItem;
}

void Client::processConnectCommand(const QVariant &arguments)
{
    QVariantMap args = arguments.toMap();

    QString senderPath = args.value("sender").toString();
    QString signalSignature = args.value("signal").toString();
    QString receiverPath = args.value("receiver").toString();
    QString slotSignature = args.value("slot").toString();

    bool success = createConnection(senderPath, signalSignature, receiverPath, slotSignature);

    QString msg;
    if (success) {
        msg = QString("Connected %1:%2->%3:%4").arg(senderPath).arg(signalSignature)
                  .arg(receiverPath).arg(slotSignature);
    } else {
        msg = QString("Failed to connect %1:%2->%3:%4").arg(senderPath).arg(signalSignature)
                  .arg(receiverPath).arg(slotSignature);
    }

    pConsole->addMessage(msg);
}
