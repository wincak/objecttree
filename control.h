#pragma once

#include <QObject>
#include <QPointer>
#include <QStandardItemModel>

class Client;
class Component;
class ConnectionTableModel;
class PropertyModel;
class SocketTableModel;
class TreeModel;

class Control : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString currentScope READ currentScope WRITE setCurrentScope NOTIFY currentScopeChanged)
    Q_PROPERTY(QString selectedComponent READ selectedComponent WRITE setSelectedComponent NOTIFY selectedComponentChanged)
    Q_PROPERTY(QList<QObject*> scopeComponents READ scopeComponents NOTIFY scopeComponentsChanged)
    Q_PROPERTY(QList<QObject*> scopeConnections READ scopeConnections NOTIFY scopeConnectionsChanged)

    Q_PROPERTY(bool currentScopeIsRoot READ currentScopeIsRoot NOTIFY currentScopeIsRootChanged)
    Q_PROPERTY(bool selectedComponentHasChildren READ selectedComponentHasChildren
               NOTIFY selectedComponentHasChildrenChanged)

    Q_PROPERTY(QStringList componentTypes READ componentTypes CONSTANT)

public:
    explicit Control(Client* client, QObject* parent = nullptr);

    QString currentScope() const;
    QString selectedComponent() const;
    QList<QObject*> scopeComponents() const;
    QList<QObject*> scopeConnections() const;

    bool currentScopeIsRoot() const;
    bool selectedComponentHasChildren() const;

    QPointer<TreeModel> hierarchyModel() const;

    QPointer<PropertyModel> propertyModel(const QString& path, QObject* parent) const;
    QPointer<SocketTableModel> socketModel(const QString& path, QObject* parent) const;
    QPointer<ConnectionTableModel> connectionModel(const QString& path, QObject* parent) const;

    Q_INVOKABLE QStringList childrenModel(const QString& parent) const;
    Q_INVOKABLE QStringList signalsModel(const QString& path) const;
    Q_INVOKABLE QStringList slotsModel(const QString& path) const;
    QVariantMap serializedConfig(const QString& path) const;

    QStringList componentTypes() const;

    Q_INVOKABLE void createComponent(const QString& scope, const QString& type, int xPos, int yPos);
    Q_INVOKABLE void removeComponent(const QString& path);

    Q_INVOKABLE bool createConnection(const QString& sender, const QString& signal,
                                      const QString& receiver, const QString& slot);

    void issueCommand(const QString& command);

signals:
    void currentScopeChanged(QString currentScope);
    void selectedComponentChanged(QString selectedComponent);
    void scopeComponentsChanged(QList<QObject*> scopeComponents);
    void scopeConnectionsChanged(QList<QObject*> scopeConnections);

    void currentScopeIsRootChanged(bool);
    void selectedComponentHasChildrenChanged(bool);

public slots:
    void setCurrentScope(QString path);
    Q_INVOKABLE void setUpperScopeAsCurrent();
    void setSelectedComponent(QString path);
    void deselectAllComponents();

private:
    void reloadScopeComponents();
    void reloadScopeConnections();
    void reloadHierarchyModel();

private:
    QPointer<Client> m_client;

    QString m_currentScope;
    QString m_selectedComponent;
    QList<QObject*> m_scopeComponents;
    QList<QObject*> m_scopeConnections;

    QPointer<TreeModel> m_hierarchyModel;
};
