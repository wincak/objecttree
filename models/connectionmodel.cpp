#include "connectionmodel.h"

#include "connection.h"

ConnectionModel::ConnectionModel(Connection* connection, QObject* parent)
    : QObject(parent)
    , m_connection(connection)
{
    Q_ASSERT(connection);

    connect(connection, &Connection::startXPosChanged, this, &ConnectionModel::startXPosChanged);
    connect(connection, &Connection::startYPosChanged, this, &ConnectionModel::startYPosChanged);
    connect(connection, &Connection::endXPosChanged, this, &ConnectionModel::endXPosChanged);
    connect(connection, &Connection::endYPosChanged, this, &ConnectionModel::endYPosChanged);

    connect(connection, &Connection::isValidChanged, this, &ConnectionModel::validChanged);
}

int ConnectionModel::startXPos() const
{
    return m_connection->startXPos();
}

int ConnectionModel::startYPos() const
{
    return m_connection->startYPos();
}

int ConnectionModel::endXPos() const
{
    return m_connection->endXPos();
}

int ConnectionModel::endYPos() const
{
    return m_connection->endYPos();
}

bool ConnectionModel::isValid() const
{
    if (m_connection.isNull())
        return false;

    return m_connection->isValid();
}
