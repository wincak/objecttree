#include "socketmodel.h"

#include "socket.h"

SocketModel::SocketModel(Socket *socket, QObject *parent)
    : QObject(parent)
    , m_socket(socket)
{
    Q_ASSERT(socket);

    m_outsideEdge = socket->outsideEdge();
    m_outsideEdgeOffset = socket->outsideEdgeOffset();
    m_signature = socket->signature();

    connect(socket, &Socket::outsideEdgeChanged, this, &SocketModel::outsideEdgeChanged);
    connect(socket, &Socket::outsideEdgeOffsetChanged, this, &SocketModel::outsideEdgeOffsetChanged);
}

Qt::Edge SocketModel::outsideEdge() const
{
    return m_outsideEdge;
}

int SocketModel::outsideEdgeOffset() const
{
    return m_outsideEdgeOffset;
}

QString SocketModel::signature() const
{
    return m_signature;
}
