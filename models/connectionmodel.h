#pragma once

#include <QObject>
#include <QPointer>

class Connection;

class ConnectionModel : public QObject {
    Q_OBJECT

    Q_PROPERTY(int startXPos READ startXPos NOTIFY startXPosChanged)
    Q_PROPERTY(int startYPos READ startYPos NOTIFY startYPosChanged)
    Q_PROPERTY(int endXPos READ endXPos NOTIFY endXPosChanged)
    Q_PROPERTY(int endYPos READ endYPos NOTIFY endYPosChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY validChanged)

public:
    explicit ConnectionModel(Connection* connection, QObject* parent);

    int startXPos() const;
    int startYPos() const;
    int endXPos() const;
    int endYPos() const;
    bool isValid() const;

signals:
    void startXPosChanged(int startXPos);
    void startYPosChanged(int startYPos);
    void endXPosChanged(int endXPos);
    void endYPosChanged(int endYPos);
    void validChanged(bool isValid);

private:
    QPointer<Connection> m_connection;
};
