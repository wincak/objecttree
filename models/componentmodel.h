#pragma once

#include <QObject>

#include <QPointer>

class Component;

class ComponentModel : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString className READ className CONSTANT STORED true)
    Q_PROPERTY(QString objectName READ objectName WRITE setObjectName NOTIFY objectNameChanged STORED true)
    Q_PROPERTY(QString path READ path NOTIFY pathChanged)

    Q_PROPERTY(QList<QObject*> sockets READ sockets NOTIFY socketsChanged)

    Q_PROPERTY(int xPos READ xPos WRITE setXPos NOTIFY xPosChanged)
    Q_PROPERTY(int yPos READ yPos WRITE setYPos NOTIFY yPosChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)

public:
    explicit ComponentModel(Component* component, QObject* parent = nullptr);

    QString className() const;
    QString objectName() const;
    QString path() const;

    QList<QObject*> sockets() const;

    int xPos() const;
    int yPos() const;
    int width() const;
    int height() const;

signals:
    void objectNameChanged(QString objectName);
    void pathChanged(QString path);

    void socketsChanged(QList<QObject*> sockets);

    void xPosChanged(int xPos);
    void yPosChanged(int yPos);
    void widthChanged(int width);
    void heightChanged(int height);

public slots:
    void setObjectName(QString objectName);

    void setXPos(int xPos);
    void setYPos(int yPos);
    void setWidth(int width);
    void setHeight(int height);

private:
    QPointer<Component> m_component;
    QList<QObject*> m_sockets;
};
