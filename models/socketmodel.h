#pragma once

#include <QObject>

class Socket;

class SocketModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Qt::Edge outsideEdge READ outsideEdge NOTIFY outsideEdgeChanged)
    Q_PROPERTY(int outsideEdgeOffset READ outsideEdgeOffset NOTIFY outsideEdgeOffsetChanged)
    Q_PROPERTY(QString signature READ signature CONSTANT)

public:
    explicit SocketModel(Socket* socket, QObject* parent = nullptr);

    Qt::Edge outsideEdge() const;
    int outsideEdgeOffset() const;

    QString signature() const;

signals:
    void outsideEdgeChanged(Qt::Edge outsideEdge);
    void outsideEdgeOffsetChanged(int outsideEdgeOffset);

private:
    Socket* m_socket;

    Qt::Edge m_outsideEdge;
    int m_outsideEdgeOffset;
    QString m_signature;
};

