#include "componentmodel.h"

#include "component.h"
#include "socket.h"
#include "socketmodel.h"

ComponentModel::ComponentModel(Component* component, QObject* parent)
    : QObject(parent)
    , m_component(component)
{
    Q_ASSERT(m_component);

    QVector<Socket*> sockets = m_component->objectSockets();
    for (auto socket : sockets)
        m_sockets.append(new SocketModel(socket, this));

    if (m_component) {
        connect(m_component, &Component::objectNameChanged, this, &ComponentModel::objectNameChanged);
        connect(m_component, &Component::pathChanged, this, &ComponentModel::pathChanged);
        connect(m_component, &Component::xPosChanged, this, &ComponentModel::xPosChanged);
        connect(m_component, &Component::yPosChanged, this, &ComponentModel::yPosChanged);
        connect(m_component, &Component::widthChanged, this, &ComponentModel::widthChanged);
        connect(m_component, &Component::heightChanged, this, &ComponentModel::heightChanged);
    }
}

QString ComponentModel::className() const
{
    if (m_component.isNull())
        return QString();

    return m_component->className();
}

QString ComponentModel::objectName() const
{
    if (m_component.isNull())
        return QString();

    return m_component->objectName();
}

QString ComponentModel::path() const
{
    if (m_component.isNull())
        return QString();

    return m_component->path();
}

QList<QObject *> ComponentModel::sockets() const
{
    return m_sockets;
}

int ComponentModel::xPos() const
{
    if (m_component.isNull())
        return (0);

    return m_component->xPos();
}

int ComponentModel::yPos() const
{
    if (m_component.isNull())
        return (0);

    return m_component->yPos();
}

int ComponentModel::width() const
{
    if (m_component.isNull())
        return (0);

    return m_component->width();
}

int ComponentModel::height() const
{
    if (m_component.isNull())
        return (0);

    return m_component->height();
}

void ComponentModel::setObjectName(QString objectName)
{
    if (m_component)
        m_component->setObjectName(objectName);
}

void ComponentModel::setXPos(int xPos)
{
    if (m_component)
        m_component->setXPos(xPos);
}

void ComponentModel::setYPos(int yPos)
{
    if (m_component)
        m_component->setYPos(yPos);
}

void ComponentModel::setWidth(int width)
{
    if (m_component)
        m_component->setWidth(width);
}

void ComponentModel::setHeight(int height)
{
    if (m_component)
        m_component->setHeight(height);
}
