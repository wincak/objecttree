#pragma once

#include <QObject>

#include <QPointer>
#include <QMetaMethod>

class Component;

enum class SocketType {
    Signal,
    Slot
};

class Socket : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Qt::Edge outsideEdge READ outsideEdge WRITE setOutsideEdge NOTIFY outsideEdgeChanged)
    Q_PROPERTY(int outsideEdgeOffset READ outsideEdgeOffset NOTIFY outsideEdgeOffsetChanged)
    Q_PROPERTY(int outsideXPos READ outsideXPos NOTIFY outsideXPosChanged)
    Q_PROPERTY(int outsideYPos READ outsideYPos NOTIFY outsideYPosChanged)
    Q_PROPERTY(SocketType type READ type CONSTANT)
    Q_PROPERTY(QString signature READ signature CONSTANT)
    Q_PROPERTY(int insideXPos READ insideXPos WRITE setInsideXPos NOTIFY insideXPosChanged)
    Q_PROPERTY(int insideYPos READ insideYPos WRITE setInsideYPos NOTIFY insideYPosChanged)

public:
    explicit Socket(Component* parent, QMetaMethod metaMethod, Qt::Edge outsideEdge, int outsideEdgeOffset);
    virtual ~Socket();

    Qt::Edge outsideEdge() const;
    void setOutsideEdge(Qt::Edge edge);

    int outsideEdgeOffset() const;
    void setOutsideEdgeOffset(int offset);

    int outsideXPos() const;
    int outsideYPos() const;

    SocketType type() const;
    QString signature() const;
    QMetaMethod metaMethod() const;

    int insideXPos() const;
    void setInsideXPos(int insideXPos);

    int insideYPos() const;
    void setInsideYPos(int insideYPos);

signals:
    void outsideEdgeChanged(Qt::Edge outsideEdge);
    void outsideEdgeOffsetChanged(int outsideEdgeOffset);
    void outsideXPosChanged(int outsideXPos);
    void outsideYPosChanged(int outsideYPos);
    void insideXPosChanged(int insideXPos);
    void insideYPosChanged(int insideYPos);

private slots:
    void updateOutsideXPos();
    void updateOutsideYPos();

private:
    Qt::Edge m_outsideEdge;
    int m_outsideEdgeOffset;
    int m_outsideXPos;
    int m_outsideYPos;
    SocketType m_type;
    QString m_signature;
    QMetaMethod m_metaMethod;
    int m_insideXPos;
    int m_insideYPos;
};
