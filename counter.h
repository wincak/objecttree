#ifndef COUNTER_H
#define COUNTER_H

#include "component.h"

class Counter : public Component
{
    Q_OBJECT

    Q_PROPERTY(bool decrementing READ decrementing WRITE setDecrementing NOTIFY decrementingChanged STORED true)
    Q_PROPERTY(int resetValue READ resetValue WRITE setResetValue NOTIFY resetValueChanged STORED true)
    Q_PROPERTY(int step READ step WRITE setStep NOTIFY stepChanged STORED true)
    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged STORED false)

public:
    Counter(Component* parent = nullptr);

    int value() const;
    bool decrementing() const;
    int resetValue() const;
    int step() const;

public slots:
    void setValue(int value);
    void setDecrementing(bool decrementing);
    void setResetValue(int resetValue);
    void setStep(int step);

    void process();
    void reset();

signals:
    void valueChanged(int);
    void decrementingChanged(bool);
    void resetValueChanged(int);
    void stepChanged(int);

private:
    int m_value;
    bool m_decrementing;
    int m_resetValue;
    int m_step;
};

#endif // COUNTER_H
