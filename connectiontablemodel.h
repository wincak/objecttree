#pragma once

#include <QAbstractTableModel>

#include <QPointer>

#include "component.h"

enum class ConnectionTableModelIndex {
    Sender = 0,
    Signal,
    Receiver,
    Slot,
    Valid,
    StartXPos,
    StartYPos,
    EndXPos,
    EndYPos,
};

class ConnectionTableModel : public QAbstractTableModel {
    Q_OBJECT

public:
    static int OptionsRole;

    explicit ConnectionTableModel(QObject* parent, Component* component);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
    QPointer<Component> m_component;
};
