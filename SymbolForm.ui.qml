import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    property alias objectNameLabel: objectNameLabel
    property alias classNameLabel: classNameLabel
    property alias rect: rect
    property alias center: center
    property alias mouseArea: mouseArea

    Rectangle {
        id: rect
        color: "gray"
        anchors.fill: parent

        Text {
            id: classNameLabel
            text: "className"
            horizontalAlignment: Text.AlignHCenter
            anchors.top: objectNameLabel.bottom
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            font.pixelSize: 12
        }
        TextEdit {
            id: objectNameLabel
            height: 20
            text: "objectName"
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            font.pixelSize: 14
            selectByMouse: true
        }
        MouseArea {
            id: mouseArea
            anchors.fill: parent
            propagateComposedEvents: false
        }
    }

    LinkSocket {
        id: center
        x: parent.width / 2 - center.width / 2
        y: parent.height / 2 - center.height / 2
    }

    states: [
        State {
            name: "selected"
            PropertyChanges {
                target: rect
                color: "#565656"
                border.width: 2
            }
            PropertyChanges {
                target: center
                color: "red"
            }
        }
    ]
}




/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
