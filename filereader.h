#ifndef FILEREADER_H
#define FILEREADER_H

#include "component.h"

class FileReader : public Component
{
    Q_OBJECT

    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged STORED true)
    Q_PROPERTY(QString contents READ contents NOTIFY contentsChanged STORED false)

public:
    FileReader(Component* parent = nullptr);

    QString path() const;

    QString contents() const;

public slots:
    void setPath(QString path);

    void reload();

signals:
    void pathChanged(QString);

    void contentsChanged(QString);

private:
    void updateContents(QString contents);

private:
    QString m_path;

    QString m_contents;
};

#endif // FILEREADER_H
