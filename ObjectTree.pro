#-------------------------------------------------
#
# Project created by QtCreator 2018-03-08T22:26:49
#
#-------------------------------------------------

QT       += core gui designer quickwidgets mqtt

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ObjectTree
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    client.cpp \
    connectiontablemodel.cpp \
    connectiontablemodeldelegate.cpp \
    console.cpp \
    consoleoutput.cpp \
    control.cpp \
        main.cpp \
        mainwindow.cpp \
    component.cpp \
    models/componentmodel.cpp \
    models/connectionmodel.cpp \
    models/socketmodel.cpp \
    mqtt/mqttinput.cpp \
    mqttclient.cpp \
    sockettablemodel.cpp \
    treemodel.cpp \
    timer.cpp \
    propertymodel.cpp \
    connection.cpp \
    propertychangenotifier.cpp \
    counter.cpp \
    filereader.cpp \
    socket.cpp

HEADERS += \
    client.h \
    connectiontablemodel.h \
    connectiontablemodeldelegate.h \
    console.h \
    consoleoutput.h \
    control.h \
        mainwindow.h \
    component.h \
    models/componentmodel.h \
    models/connectionmodel.h \
    models/socketmodel.h \
    mqtt/mqttinput.h \
    mqttclient.h \
    sockettablemodel.h \
    treemodel.h \
    timer.h \
    propertymodel.h \
    connection.h \
    propertychangenotifier.h \
    counter.h \
    filereader.h \
    socket.h

FORMS += \
        mainwindow.ui

DISTFILES += \
    CreateConnectionDialog.qml \
    CreateConnectionDialogForm.ui.qml \
    SchematicForm.ui.qml \
    Schematic.qml \
    Symbol.qml \
    SymbolForm.ui.qml \
    Wire.qml \
    LinkSocket.qml \
    Link.qml

RESOURCES += \
    qml.qrc
