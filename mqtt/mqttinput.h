#pragma once

#include "component.h"

class QMqttSubscription;

class MqttInput : public Component {
    Q_OBJECT

    Q_PROPERTY(QString client READ client WRITE setClient NOTIFY clientChanged STORED true)
    Q_PROPERTY(QString topic READ topic WRITE setTopic NOTIFY topicChanged STORED true)

    Q_PROPERTY(QString state READ state NOTIFY stateChanged STORED false)

public:
    MqttInput(Component* parent = nullptr);

    QString client() const;
    QString topic() const;

    QString state() const;

public slots:
    void setClient(QString client);
    void setTopic(QString topic);

    Q_INVOKABLE void subscribe();
    Q_INVOKABLE void unsubscribe();

signals:
    void clientChanged(QString client);
    void topicChanged(QString topic);

    void stateChanged(QString state);

protected:
    void setState(const QString& state);

private:
    QString m_client;
    QString m_topic;

    QString m_state;

    QMqttSubscription* m_subscription;
};
