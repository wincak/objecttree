#include "mqttinput.h"

#include <QMqttClient>
#include <QMqttSubscription>

#include "mqttclient.h"

MqttInput::MqttInput(Component* parent)
    : Component(parent)
{
}

QString MqttInput::client() const
{
    return m_client;
}

QString MqttInput::topic() const
{
    return m_topic;
}

QString MqttInput::state() const
{
    return m_state;
}

void MqttInput::setClient(QString client)
{
    if (m_client == client)
        return;

    m_client = client;
    emit clientChanged(m_client);
}

void MqttInput::setTopic(QString topic)
{
    if (m_topic == topic)
        return;

    m_topic = topic;
    emit topicChanged(m_topic);
}

void MqttInput::subscribe()
{
    MqttClient* client = nullptr;
    auto parent = qobject_cast<Component*>(this->parent());
    if (parent) {
        auto siblings = parent->childComponents();

        for (auto sibling : siblings) {
            if (sibling->objectName() != m_client)
                continue;

            client = qobject_cast<MqttClient*>(sibling);
            break;
        }
    }
    if (!client) {
        setState("clientNotFound");
        return;
    }

    // TODO: subscribe to client, set state accordingly and start receiving messages
    //    QMqttTopicFilter topic;
    Q_ASSERT(false);
}

void MqttInput::unsubscribe()
{
    // TODO
    Q_ASSERT(false);
}

void MqttInput::setState(const QString& state)
{
    if (state == m_state)
        return;

    m_state = state;
    emit stateChanged(m_state);
}
