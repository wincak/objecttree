#ifndef SOCKETMODEL_H
#define SOCKETMODEL_H

#include <QAbstractTableModel>

#include <QPointer>

class Component;

class SocketTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum SocketIndexes {
        SocketType,
        SocketSignature,
        SocketOutsideEdge,
        SocketOutsideEdgeOffset,
        SocketInsideXPos,
        SocketInsideYPos,
    };

    explicit SocketTableModel(QObject* parent, Component* component);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
    QPointer<Component> m_component;
};

#endif // SOCKETMODEL_H
