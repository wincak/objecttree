#include "propertymodel.h"

#include <QDebug>
#include <QMetaProperty>

#include "propertychangenotifier.h"

PropertyModel::PropertyModel(QObject* parent, Component* component)
    : QAbstractTableModel(parent)
    , m_component(component)
{
    Q_ASSERT(!m_component.isNull());

    mapValueUpdates();
}

QVariant PropertyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    QVariant data;
    if (orientation != Qt::Vertical) {
        switch (section) {
        case PropertyName:
            data = QVariant::fromValue(tr("Name"));
            break;
        case PropertyValueType:
            data = QVariant::fromValue(tr("Value type"));
            break;
        case PropertyWritable:
            data = QVariant::fromValue(tr("Writable"));
            break;
        case PropertyValue:
            data = QVariant::fromValue(tr("Value"));
            break;
        }
    } else {
        if (section >= m_component->objectProperties().count())
            return QVariant();

        data = QVariant::fromValue(section);
    }

    return data;
}

int PropertyModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    return m_component->objectProperties().count();
}

int PropertyModel::columnCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    // Name, ValueType, Mutable, Value
    return 4;
}

QVariant PropertyModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();

    QVector<QMetaProperty> objectProperties = m_component->objectProperties();
    if (index.row() >= objectProperties.count())
        return QVariant();

    QMetaProperty property = objectProperties.at(index.row());

    QVariant value;
    switch (index.column()) {
    case PropertyName:
        value = QVariant::fromValue(QString(property.name()));
        break;
    case PropertyValueType:
        value = QVariant::fromValue(QString(property.typeName()));
        break;
    case PropertyWritable:
        value = QVariant::fromValue(property.isWritable());
        break;
    case PropertyValue:
        value = QVariant::fromValue(property.read(m_component));
        break;
    }

    return value;
}

bool PropertyModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (role != Qt::EditRole)
        return false;

    QVector<QMetaProperty> objectProperties = m_component->objectProperties();
    if (index.row() >= objectProperties.count())
        return false;

    bool success = false;
    QMetaProperty property = m_component->objectProperties().at(index.row());
    if (value != property.read(m_component)) {
        success = property.write(m_component, value);
        if (success)
            emit dataChanged(index, index, QVector<int>() << role);
    }

    return success;
}

Qt::ItemFlags PropertyModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    QVector<QMetaProperty> objectProperties = m_component->objectProperties();
    if (index.row() >= objectProperties.count())
        return Qt::NoItemFlags;

    QMetaProperty property = objectProperties.at(index.row());
    Qt::ItemFlags itemFlags = Qt::NoItemFlags;
    switch (index.column()) {
    case PropertyName:
    case PropertyValueType:
    case PropertyWritable:
        itemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        break;
    case PropertyValue:
        itemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        if (property.isWritable())
            itemFlags |= Qt::ItemIsEditable;
        break;
    }

    return itemFlags;
}

void PropertyModel::mapValueUpdates()
{
    if (m_component.isNull())
        return;

    QVector<QMetaProperty> objectProperties = m_component->objectProperties();
    if (objectProperties.isEmpty())
        return;

    for (int index = 0; index < objectProperties.count(); index++) {
        QMetaProperty property = objectProperties.at(index);
        if (!property.hasNotifySignal())
            continue;

//        QMetaMethod notifySignal = property.notifySignal();
//        QMetaMethod updateSlot = this->metaObject()->method(
//                    this->metaObject()->indexOfSlot("notifyDataChange()"));
//        connect(m_component, notifySignal, this, updateSlot);

        PropertyChangeNotifier* notifier = new PropertyChangeNotifier(this, index);
        connect(m_component, property.notifySignal(), notifier, notifier->notifySlotMetaMethod());
        connect(notifier, &PropertyChangeNotifier::propertyChanged, this, &PropertyModel::notifyPropertyValueChanged);

//        QByteArray signalSignature = property.notifySignal().methodSignature();
//        bool ok = connect(m_component, SIGNAL(property.notifySignal().methodSignature()), this, SLOT(notifyDataChange()));

        // TODO: is it possible to somehow map individual notify signals without deprecated QSignalMapper? Maybe lambdas?
//        connect(m_component, property.notifySignal(), [this, index]() {
//            emit dataChanged(createIndex(index, PropertyValue), createIndex(index, PropertyValue), QVector<int>{Qt::DisplayRole});
//        });
    }
}

void PropertyModel::notifyPropertyValueChanged(int index)
{
    emit dataChanged(createIndex(index, PropertyValue), createIndex(index, PropertyValue), QVector<int>{Qt::DisplayRole});
}
