#pragma once

#include <QObject>

#define pConsole Console::instance()

class Console : public QObject {
    Q_OBJECT

    Q_PROPERTY(QStringList messages READ messages NOTIFY messagesChanged)
    Q_PROPERTY(int messageLimit READ messageLimit WRITE setMessageLimit NOTIFY messageLimitChanged)

public:
    explicit Console(QObject* parent = nullptr);

    static Console* instance();

    QStringList messages() const;
    int messageLimit() const;

signals:
    void messagesChanged(QStringList messages);
    void messageLimitChanged(int messageLimit);

public slots:
    void addMessage(const QString& message);
    void setMessageLimit(int messageLimit);

private:
    static Console* m_instance;

    QStringList m_messages;
    int m_messageLimit;
};
