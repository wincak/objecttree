#include "timer.h"

#include <QTimer>

Timer::Timer(Component *parent)
    : Component (parent)
{
    m_timer = new QTimer(this);

    connect(m_timer, &QTimer::timeout, this, &Timer::timeout);
}

int Timer::interval() const
{
    return m_timer->interval();
}

bool Timer::isSingleShot() const
{
    return m_timer->isSingleShot();
}

bool Timer::isActive() const
{
    return m_timer->isActive();
}

int Timer::remainingTime() const
{
    return m_timer->remainingTime();
}

void Timer::setInterval(int msecs)
{
    m_timer->setInterval(msecs);
}

void Timer::setSingleShot(bool singleShot)
{
    m_timer->setSingleShot(singleShot);
}

void Timer::setActive(bool active)
{
    if (active == m_timer->isActive())
        return;

    if (active) {
        m_timer->start();
    } else {
        m_timer->stop();
    }
}

void Timer::start(int msecs)
{
    m_timer->start(msecs);
}

void Timer::start()
{
    m_timer->start();
}

void Timer::stop()
{
    m_timer->stop();
}
