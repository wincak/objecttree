#include "mqttclient.h"

#include <QtMqtt/QMqttTopicFilter>

MqttClient::MqttClient(Component* parent)
    : Component(parent)
{
    m_client = new QMqttClient(this);

    connect(m_client, &QMqttClient::hostnameChanged, this, &MqttClient::hostnameChanged);
    connect(m_client, &QMqttClient::portChanged, this, &MqttClient::portChanged);

    connect(m_client, &QMqttClient::connected, this, &MqttClient::connected);
    connect(m_client, &QMqttClient::disconnected, this, &MqttClient::disconnected);
    connect(m_client, &QMqttClient::stateChanged, this, &MqttClient::processStateChange);

    connect(m_client, &QMqttClient::messageReceived, this, &MqttClient::processMessage);
}

QString MqttClient::hostname() const
{
    return m_client->hostname();
}

int MqttClient::port() const
{
    return m_client->port();
}

QString MqttClient::state() const
{
    QString stateString;

    switch (m_client->state()) {
    case QMqttClient::Disconnected:
        stateString = "disconnected";
        break;
    case QMqttClient::Connecting:
        stateString = "connecting";
        break;
    case QMqttClient::Connected:
        stateString = "connected";
    }

    return stateString;
}

QMqttSubscription* MqttClient::subscribe(const QMqttTopicFilter& topic, quint8 qos)
{
    return m_client->subscribe(topic, qos);
}

QString MqttClient::topic() const
{
    return m_topic;
}

void MqttClient::setHostname(QString hostname)
{
    m_client->setHostname(hostname);
}

void MqttClient::setPort(int port)
{
    m_client->setPort(port);
}

void MqttClient::connectToHost()
{
    m_client->connectToHost();
}

void MqttClient::disconnectFromHost()
{
    m_client->disconnectFromHost();
}

void MqttClient::setTopic(QString topic)
{
    if (m_topic == topic)
        return;

    m_topic = topic;
    emit topicChanged(m_topic);

    // TODO: automatic subscription is not very consistent with having to connect to broker manually
    // This needs to be improved
    QMqttSubscription* sub = m_client->subscribe(QMqttTopicFilter(m_topic));
    if (!sub) {
        qWarning("Failed to subscribe to topic");
    } else {
        qDebug("Subscribed to topic using filter: %s", qPrintable(sub->topic().filter()));
    }
    // TODO: subscription needs to be stored. This way calling setTopic again results in multiple
    // subscription
}

void MqttClient::processMessage(const QByteArray& message, const QMqttTopicName& topic)
{
    QString string;
    string = topic.name() + QString::fromUtf8(message);

    emit messageReceived(string);
}

void MqttClient::processStateChange(QMqttClient::ClientState state)
{
    Q_UNUSED(state)

    QString stateString = this->state();

    emit stateChanged(stateString);
}
