#include "connection.h"

#include "component.h"
#include "socket.h"

#include <QDebug>

Connection::Connection(QObject* parent)
    : QObject(parent)
{
}

Connection::Connection(QObject *parent, QPointer<Socket> sender, QPointer<Socket> receiver)
    : QObject(parent)
    , m_sender(sender)
    , m_receiver(receiver)
{
    Q_ASSERT(m_sender && m_receiver);
    if (!m_sender || !m_receiver)
        return;

    attemptConnect();

    connect(m_sender, &Socket::outsideXPosChanged, this, &Connection::startXPosChanged);
    connect(m_sender, &Socket::outsideYPosChanged, this, &Connection::startYPosChanged);
    connect(m_receiver, &Socket::outsideXPosChanged, this, &Connection::endXPosChanged);
    connect(m_receiver, &Socket::outsideYPosChanged, this, &Connection::endYPosChanged);
}

QPointer<Socket> Connection::sender() const
{
    return m_sender;
}

QPointer<Socket> Connection::receiver() const
{
    return m_receiver;
}

bool Connection::isValid() const
{
    if (m_handle)
        return true;

    return false;
}

int Connection::startXPos() const
{
    if (m_sender.isNull())
        return 0;

    return m_sender->outsideXPos();
}

int Connection::startYPos() const
{
    if (m_sender.isNull())
        return 0;

    return m_sender->outsideYPos();
}

int Connection::endXPos() const
{
    if (m_receiver.isNull())
        return 0;

    return m_receiver->outsideXPos();
}

int Connection::endYPos() const
{
    if (m_receiver.isNull())
        return 0;

    return m_receiver->outsideYPos();
}

void Connection::attemptConnect()
{
    // Disconnect existing signal - should not happen
    Q_ASSERT(!m_handle);
    if (m_handle) {
        disconnect(m_handle);
        emit disconnected();
    }

    // TODO: check validity
//    if (!m_sender || !m_signal.isValid() || !m_receiver || !m_slot.isValid())
//        return;

    m_handle = connect(m_sender->parent(), m_sender->metaMethod(),
                       m_receiver->parent(), m_receiver->metaMethod());
    if (m_handle)
        emit connected();
}

QString Connection::signalSignature() const
{
    return m_sender->signature();
}

QString Connection::slotSignature() const
{
    return m_receiver->signature();
}
