#include "filereader.h"

#include <QFile>

FileReader::FileReader(Component* parent)
    : Component(parent)
{

}

QString FileReader::path() const
{
    return m_path;
}

QString FileReader::contents() const
{
    return m_contents;
}

void FileReader::setPath(QString path)
{
    if (path == m_path)
        return;

    m_path = path;

    emit pathChanged(m_path);
}

void FileReader::reload()
{
    QString contents;

    QFile file(m_path);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        contents = QString::fromLocal8Bit(file.readAll());
    }

    updateContents(contents);
}

void FileReader::updateContents(QString contents)
{
    if (contents == m_contents)
        return;

    m_contents = contents;

    emit contentsChanged(m_contents);
}
