#pragma once

#include <QObject>

#include <QPointer>
#include <QVariantMap>
#include <QVector>

class Connection;
class Socket;

class Component : public QObject
{
    Q_OBJECT

public:
    Component(Component* parent = nullptr);
    ~Component();

    QString className() const;
    QString path() const;

    QPointer<Component> child(QString path) const;
    int childCount() const;
    bool addChild(QPointer<Component> child);
    bool removeChild(const QString& name);
    int childNumber() const;

    bool createConnection(Socket* sender, Socket* receiver);

    QPointer<Socket> socket(const QString& signature) const;

    QList<QPointer<Component>> childComponents() const;

    Q_INVOKABLE QVariant objectProperty(const QString& propertyId);
    Q_INVOKABLE bool setObjectProperty(const QString& propertyId, const QVariant& value);

    QVector<QMetaProperty> objectProperties() const;

    // Consider using just as protected functions and expose only object sockets
    QVector<QMetaMethod> objectSignals() const;
    QVector<QMetaMethod> objectSlots() const;

    QVector<QPointer<Connection>> objectConnections() const;
    QVector<Socket*> objectSockets() const;

    // TODO: consider renaming as "storeConfig" or similar - it's not value serialization
    Q_INVOKABLE QVariantMap serialize() const;

    static Component* instantiate(const QString* className, Component* parent);

    int xPos() const;
    void setXPos(int xPos);

    int yPos() const;
    void setYPos(int yPos);

    int width() const;
    void setWidth(int width);

    int height() const;
    void setHeight(int height);

signals:
    void objectNameChanged(QString);
    void pathChanged(QString);

    void connectionsChanged();

    void xPosChanged(int);
    void yPosChanged(int);
    void widthChanged(int);
    void heightChanged(int);

    void childAdded(QString name);
    void childRemoved(QString name);

protected slots:
    void initializeSockets();

private:
    QList<QPointer<Component>> m_childItems;
    QVector<QPointer<Connection>> m_connections;
    QVector<Socket*> m_sockets;

    int m_xPos;
    int m_yPos;
    int m_width;
    int m_height;
};
