#include "connectiontablemodeldelegate.h"

#include <QComboBox>

#include "connectiontablemodel.h"

ConnectionTableModelDelegate::ConnectionTableModelDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
{
}

QWidget* ConnectionTableModelDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option,
    const QModelIndex& index) const
{
    // Create the combobox and populate it
    QComboBox* cb = new QComboBox(parent);

    switch (index.column()) {
    case ColumnSender:
        cb->addItem(tr("<sender>"));
        cb->addItems(index.data(ConnectionTableModel::OptionsRole).toStringList());
        break;
    case ColumnSignal:
        cb->addItem(tr("<signal>"));
        cb->addItems(index.data(ConnectionTableModel::OptionsRole).toStringList());
        break;
    case ColumnReceiver:
        cb->addItem(tr("<receiver>"));
        cb->addItems(index.data(ConnectionTableModel::OptionsRole).toStringList());
        break;
    case ColumnSlot:
        cb->addItem(tr("<slot>"));
        cb->addItems(index.data(ConnectionTableModel::OptionsRole).toStringList());
        break;
    default:
        return QStyledItemDelegate::createEditor(parent, option, index);
    }

    return cb;
}

void ConnectionTableModelDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    if (!combo)
        return QStyledItemDelegate::setEditorData(editor, index);

    //    if (QComboBox* cb = qobject_cast<QComboBox*>(editor)) {
    //       // get the index of the text in the combobox that matches the current value of the itenm
    //       QString currentText = index.data(Qt::EditRole).toString();
    //       int cbIndex = cb->findText(currentText);
    //       // if it is valid, adjust the combobox
    //       if (cbIndex >= 0)
    //           cb->setCurrentIndex(cbIndex);
    //    } else {
    //        QStyledItemDelegate::setEditorData(editor, index);
    //    }
}

void ConnectionTableModelDelegate::setModelData(QWidget* editor, QAbstractItemModel* model,
    const QModelIndex& index) const
{
    if (QComboBox* cb = qobject_cast<QComboBox*>(editor)) {
        // save the current text of the combo box as the current value of the item
        model->setData(index, cb->currentText(), Qt::EditRole);
    } else {
        QStyledItemDelegate::setModelData(editor, model, index);
    }
}
