import QtQuick 2.12
import QtQml 2.12

SymbolForm {
    id: component

    property var model: null

    property string className: model ? model.className : ""
    property string objectName: model ? model.objectName : ""
    property string path: model ? model.path : ""

    x: model ? model.xPos : 0
    y: model ? model.yPos : 0
    width: model ? model.width : 0
    height: model ? model.height : 0

    objectNameLabel.text: model ? model.objectName : qsTr("Object name")
    classNameLabel.text: model ? model.className : qsTr("Class name")

    // NOTE: this might cause issues if no component is selected and path is invalid
    state: control.selectedComponent === path ? "selected" : ""

    mouseArea.acceptedButtons: Qt.LeftButton | Qt.RightButton
    mouseArea.drag.target: component
    mouseArea.drag.axis: Drag.XAndYAxis
    mouseArea.hoverEnabled: true

    Binding {
        target: model
        property: "objectName"
        value: objectNameLabel.text
        when: model !== null
    }

    Binding {
        target: model
        property: "xPos"
        value: component.x
        when: model !== null
    }
    Binding {
        target: model
        property: "yPos"
        value: component.y
        when: model !== null
    }
    Binding {
        target: model
        property: "width"
        value: component.width
        when: model !== null
    }
    Binding {
        target: model
        property: "height"
        value: component.height
        when: model !== null
    }

    Repeater {
        model: component.model ? component.model.sockets : 0

        LinkSocket {
            width: 5
            height: 5
            color: "black"
            name: modelData.signature
            x: modelData.outsideEdge === Qt.LeftEdge ? 0 : (component.width - width)
            y: modelData.outsideEdgeOffset
        }
    }
}
