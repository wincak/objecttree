#pragma once

#include <QMainWindow>

#include <QPointer>

namespace Ui {
class MainWindow;
}

class Control;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void setCurrentComponent(const QModelIndex& index);

    void displayComponentProperties(const QString& path);
    void displayComponentSockets(const QString& path);
    void displayComponentSignals(const QString& path);
    void displayComponentSlots(const QString& path);
    void displayComponentSerialized(const QString& path);

    void displayScopeSerialized(const QString& path);
    void displayScopeConnections(const QString& path);

    void addConnection();
    void removeConnection();

    void updateConsoleMessages(QStringList messages);
    void issueConsoleCommand();

private:
    Ui::MainWindow* m_ui;

    QPointer<Control> m_control;
};
