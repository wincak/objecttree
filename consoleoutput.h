#pragma once

#include "component.h"

class ConsoleOutput : public Component {
    Q_OBJECT

public:
    ConsoleOutput(Component* parent = nullptr);

public slots:
    void printString(const QString& string);
    void printInteger(int integer);
};
