#include "treemodel.h"

TreeModel::TreeModel(QPointer<Component> rootItem, QObject* parent)
    : QAbstractItemModel(parent)
    , m_rootItem(rootItem)
{
}

TreeModel::~TreeModel()
{
    delete m_rootItem;
}

QVariant TreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    Component* item = getItem(index);

    return item->objectName();
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return m_rootItem->objectName();

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex& parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    Component* parentItem = getItem(parent);

    Component* childItem = parentItem->childComponents().value(row);
    if (childItem) {
        return createIndex(row, column, childItem);
    } else {
        return QModelIndex();
    }
}

QModelIndex TreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return QModelIndex();

    Component* childItem = getItem(index);
    if (!childItem)
        return QModelIndex();

    auto parentItem = qobject_cast<Component*>(childItem->parent());
    if (!parentItem)
        return QModelIndex();

    if (parentItem == m_rootItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex& parent) const
{
    Component* parentItem = getItem(parent);

    return parentItem->childCount();
}

int TreeModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)

    // Simple tree model, there is always just one column
    return 1;
}

Qt::ItemFlags TreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

bool TreeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (role != Qt::EditRole)
        return false;

    Component* item = getItem(index);
    if (!item)
        return false;

    item->setObjectName(value.toString());

    emit dataChanged(index, index);

    return true;
}

Component* TreeModel::getItem(const QModelIndex& index) const
{
    if (index.isValid()) {
        Component* item = static_cast<Component*>(index.internalPointer());
        if (item)
            return item;
    }

    return m_rootItem;
}
