import QtQuick 2.12
import QtQuick.Shapes 1.12

Item {
    width: 400
    height: 400

    property var model: null

    property int startX: model ? model.startXPos : 0
    property int startY: model ? model.startYPos : 0

    property int endX: model ? model.endXPos : 0
    property int endY: model ? model.endYPos : 0

    property bool valid: model ? model.isValid : false

    signal clicked();

    Rectangle {
        id: startRect
        width: 10
        height: 10
        x: startX - width / 2
        y: startY - height / 2
        color: "blue"
    }

    Rectangle {
        id: endRect
        width: 10
        height: 10
        x: endX - width / 2
        y: endY - height / 2
        color: "green"
    }

    Shape {
        x: startX
        y: startY
        width: endX - startX
        height: endY - startY

        ShapePath {
            strokeWidth: 2
            capStyle: ShapePath.RoundCap
            strokeColor: valid ? "black" : "red"
            strokeStyle: valid ? ShapePath.SolidLine : ShapePath.DashLine
            startX: 0
            startY: 0
            PathLine {
                x: endX - startX
                y: endY - startY
            }
        }
    }
}



