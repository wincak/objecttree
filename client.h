#pragma once

#include <QObject>

#include <QPointer>

class Component;

class Client : public QObject {
    Q_OBJECT

public:
    explicit Client(QObject* parent = nullptr);

    QPointer<Component> component(const QString& path) const;

    QStringList componentTypes() const;

    bool createComponent(const QString& scope, const QString& type, int xPos, int yPos);
    bool removeComponent(const QString& path);

    bool createConnection(const QString& sender, const QString& signal,
                          const QString& receiver, const QString& slot);

public slots:
    void issueCommand(const QString& command);

private:
    QPointer<Component> setupModelData();

    void processConnectCommand(const QVariant& arguments);


private:
    QPointer<Component> m_rootComponent;
};
