import QtQuick 2.0
import QtQuick.Controls 2.5

Rectangle {
    id: socket

    property string name: ""

    width: 5
    height: 5

    color: "black"
    border.color: "black"
    border.width: 1

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }

    ToolTip {
        parent: socket
        visible: mouseArea.containsMouse
        text: socket.name
    }
}
