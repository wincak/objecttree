#include "counter.h"

Counter::Counter(Component *parent)
    : Component(parent)
    , m_value(0)
    , m_decrementing(false)
    , m_resetValue(0)
    , m_step(1)
{
}

int Counter::value() const
{
    return m_value;
}

bool Counter::decrementing() const
{
    return m_decrementing;
}

int Counter::resetValue() const
{
    return m_resetValue;
}

int Counter::step() const
{
    return m_step;
}

void Counter::setValue(int value)
{
    if (value == m_value)
        return;

    m_value = value;

    emit valueChanged(m_value);
}

void Counter::setDecrementing(bool decrementing)
{
    if (decrementing == m_decrementing)
        return;

    m_decrementing = decrementing;

    emit decrementingChanged(decrementing);
}

void Counter::setResetValue(int resetValue)
{
    if (resetValue == m_resetValue)
        return;

    m_resetValue = resetValue;

    emit resetValueChanged(m_resetValue);
}

void Counter::setStep(int step)
{
    if (step == m_step)
        return;

    m_step = step;

    emit stepChanged(m_step);
}

void Counter::process()
{
    int newValue;
    if (m_decrementing) {
        newValue = m_value - m_step;
    } else {
        newValue = m_value + m_step;
    }

    if (newValue == m_value)
        return;

    m_value = newValue;

    emit valueChanged(m_value);
}

void Counter::reset()
{
    m_value = m_resetValue;
}
