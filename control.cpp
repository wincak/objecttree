#include "control.h"

#include <QMetaMethod>

#include "client.h"
#include "component.h"
#include "connection.h"
#include "connectiontablemodel.h"
#include "propertymodel.h"
#include "sockettablemodel.h"
#include "treemodel.h"

#include <models/componentmodel.h>
#include <models/connectionmodel.h>

Control::Control(Client* client, QObject* parent)
    : QObject(parent)
    , m_client(client)
{
    if (!client)
        qFatal("Client is null");

    setCurrentScope("/");

    // TODO: needs to be updated dynamically
    reloadHierarchyModel();
}

QString Control::currentScope() const
{
    return m_currentScope;
}

QString Control::selectedComponent() const
{
    return m_selectedComponent;
}

QList<QObject*> Control::scopeComponents() const
{
    return m_scopeComponents;
}

QList<QObject*> Control::scopeConnections() const
{
    return m_scopeConnections;
}

bool Control::currentScopeIsRoot() const
{
    return m_currentScope == '/';
}

bool Control::selectedComponentHasChildren() const
{
    auto component = m_client->component(m_selectedComponent);
    if (component.isNull())
        return false;

    return component->childCount() > 0;
}

QPointer<TreeModel> Control::hierarchyModel() const
{
    return m_hierarchyModel;
}

QPointer<PropertyModel> Control::propertyModel(const QString& path, QObject* parent) const
{
    Q_ASSERT(parent);

    QPointer<Component> component = m_client->component(path);
    if (!component)
        return {};

    QPointer<PropertyModel> model = new PropertyModel(parent, component);

    return model;
}

QPointer<SocketTableModel> Control::socketModel(const QString& path, QObject* parent) const
{
    Q_ASSERT(parent);

    QPointer<Component> component = m_client->component(path);
    if (!component)
        return {};

    QPointer<SocketTableModel> model = new SocketTableModel(parent, component);

    return model;
}

QPointer<ConnectionTableModel> Control::connectionModel(const QString& path, QObject* parent) const
{
    Q_ASSERT(parent);

    QPointer<Component> component = m_client->component(path);
    if (!component)
        return {};

    QPointer<ConnectionTableModel> model = new ConnectionTableModel(parent, component);

    return model;
}

QStringList Control::childrenModel(const QString &parent) const
{
    QPointer<Component> component = m_client->component(parent);
    if (!component)
        return {};

    QStringList paths;
    auto children = component->childComponents();
    for (const auto& child : children) {
        paths << child->path();
    }

    return paths;
}

QStringList Control::signalsModel(const QString& path) const
{
    QPointer<Component> component = m_client->component(path);
    if (!component)
        return {};

    QStringList list;
    QVector<QMetaMethod> objectSignals = component->objectSignals();
    for (auto signal : objectSignals) {
        list.append(signal.methodSignature());
    }

    return list;
}

QStringList Control::slotsModel(const QString& path) const
{
    QPointer<Component> component = m_client->component(path);
    if (!component)
        return {};

    QStringList list;
    QVector<QMetaMethod> objectSlots = component->objectSlots();
    for (auto slot : objectSlots) {
        list.append(slot.methodSignature());
    }

    return list;
}

QVariantMap Control::serializedConfig(const QString& path) const
{
    QPointer<Component> component = m_client->component(path);
    if (!component)
        return {};

    QVariantMap config = component->serialize();

    return config;
}

QStringList Control::componentTypes() const
{
    return m_client->componentTypes();
}

void Control::createComponent(const QString& scope, const QString& type, int xPos, int yPos)
{
    m_client->createComponent(scope, type, xPos, yPos);
}

void Control::removeComponent(const QString& path)
{
    m_client->removeComponent(path);
}

bool Control::createConnection(const QString &sender, const QString &signal, const QString &receiver, const QString &slot)
{
    bool success = m_client->createConnection(sender, signal, receiver, slot);
    if (success)
        reloadScopeConnections();

    return success;
}

void Control::issueCommand(const QString &command)
{
    m_client->issueCommand(command);
}

void Control::setSelectedComponent(QString path)
{
    if (m_selectedComponent == path)
        return;

    // Remove trailing slash(es) from path - there should be none
    if (path.endsWith('/')) {
        qCritical("Selected component path %s ends with a slash", qPrintable(path));
        return;
    }
    // Path should start with a slash
    if (!path.startsWith('/')) {
        qCritical("Selected component path %s does not start with a slash", qPrintable(path));
        return;
    }

    QPointer<Component> selected = m_client->component(path);
    if (selected.isNull()) {
        qWarning("Selected component %s is not available", qPrintable(path));
        return;
    }

    // Scope
    QString scopePath;
    auto scope = qobject_cast<Component*>(selected->parent());
    if (!scope) {
        qCritical("Selected item %s scope is null", qPrintable(path));
        scopePath = '/';
    } else {
        scopePath = scope->path();
    }
    setCurrentScope(scopePath);

    m_selectedComponent = path;
    emit selectedComponentChanged(m_selectedComponent);

    bool hasChildren = selectedComponentHasChildren();
    emit selectedComponentHasChildrenChanged(hasChildren);
}

void Control::deselectAllComponents()
{
    if (m_selectedComponent.isEmpty())
        return;

    m_selectedComponent.clear();
    emit selectedComponentChanged(m_selectedComponent);
}

void Control::setCurrentScope(QString path)
{
    if (m_currentScope == path)
        return;

    QPointer<Component> currentScope = m_client->component(m_currentScope);
    if (currentScope) {
        disconnect(currentScope, &Component::childAdded, this, &Control::reloadScopeComponents);
        disconnect(currentScope, &Component::childRemoved, this, &Control::reloadScopeComponents);
    }

    // TODO: sanity check
    QPointer<Component> scope = m_client->component(path);
    if (scope.isNull()) {
        qCritical("Current scope %s is null", qPrintable(path));
        return;
    }

    connect(scope, &Component::childAdded, this, &Control::reloadScopeComponents);
    connect(scope, &Component::childRemoved, this, &Control::reloadScopeComponents);

    m_currentScope = path;
    emit currentScopeChanged(m_currentScope);

    bool isRoot = currentScopeIsRoot();
    emit currentScopeIsRootChanged(isRoot);

    reloadScopeComponents();
    reloadScopeConnections();
}

void Control::setUpperScopeAsCurrent()
{
    QString upperScope = m_currentScope;
    upperScope.truncate(m_currentScope.lastIndexOf('/'));
    if (upperScope.isEmpty())
        upperScope = '/';

    setCurrentScope(upperScope);
}

void Control::reloadScopeComponents()
{
    QPointer<Component> scope = m_client->component(m_currentScope);

    // Delete all existing models
    for (auto item : m_scopeComponents)
        [item]() { item->deleteLater(); };

    QList<QObject*> models;
    if (scope.isNull()) {
        qCritical("Failed to load scope components. Scope is null");
    } else {
        // Create new list
        QList<QPointer<Component>> list = scope->childComponents();
        for (auto item : list) {
            if (item.isNull())
                continue;

            auto model = new ComponentModel(item, this);
            models.append(model);
        }
    }

    m_scopeComponents = models;
    emit scopeComponentsChanged(m_scopeComponents);
}

void Control::reloadScopeConnections()
{
    QPointer<Component> scope = m_client->component(m_currentScope);

    // Delete all existing models
    for (auto item : m_scopeConnections)
        [item]() { item->deleteLater(); };

    QList<QObject*> models;
    if (scope.isNull()) {
        qCritical("Failed to load scope connections. Scope is null");
    } else {
        // Create new list
        QVector<QPointer<Connection>> list = scope->objectConnections();
        for (auto item : list) {
            if (item.isNull())
                continue;

            auto model = new ConnectionModel(item, this);
            models.append(model);
        }
    }

    m_scopeConnections = models;
    emit scopeConnectionsChanged(m_scopeConnections);
}

void Control::reloadHierarchyModel()
{
    if (m_hierarchyModel)
        m_hierarchyModel->deleteLater();

    QPointer<Component> root = m_client->component("/");
    if (root.isNull()) {
        qCritical("Failed to get component hierarchy root");
        return;
    }

    m_hierarchyModel = new TreeModel(root.data(), this);
}
