#pragma once

#include <QStyledItemDelegate>

class ConnectionTableModelDelegate : public QStyledItemDelegate {
    Q_OBJECT

public:
    enum Columns {
        ColumnSender = 0,
        ColumnSignal = 1,
        ColumnReceiver = 2,
        ColumnSlot = 3,
    };

    ConnectionTableModelDelegate(QObject* parent = nullptr);

    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
        const QModelIndex& index) const override;

    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const override;

    virtual void setModelData(QWidget* editor, QAbstractItemModel* model,
        const QModelIndex& index) const override;
};
