#include "sockettablemodel.h"

#include "component.h"
#include "socket.h"

SocketTableModel::SocketTableModel(QObject *parent, Component *component)
    : QAbstractTableModel(parent)
    , m_component(component)
{

}

QVariant SocketTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    QVariant data;
    if (orientation != Qt::Vertical) {
        switch (section) {
        case SocketType:
            data = QVariant::fromValue(tr("Type"));
            break;
        case SocketSignature:
            data = QVariant::fromValue(tr("Signature"));
            break;
        case SocketOutsideEdge:
            data = QVariant::fromValue(tr("Outside edge"));
            break;
        case SocketOutsideEdgeOffset:
            data = QVariant::fromValue(tr("Outside edge offset"));
            break;
        case SocketInsideXPos:
            data = QVariant::fromValue(tr("Inside xPos"));
            break;
        case SocketInsideYPos:
            data = QVariant::fromValue(tr("Inside yPos"));
            break;
        }
    } else {
        if (section >= m_component->objectSockets().count())
            return QVariant();

        data = QVariant::fromValue(section);
    }

    return data;
}

int SocketTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_component->objectSockets().count();
}

int SocketTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    // SocketIndexes enum has seven values
    return 7;
}

QVariant SocketTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();

    QVector<Socket*> objectSockets = m_component->objectSockets();
    if (index.row() > objectSockets.count())
        return QVariant();

    Socket* socket = objectSockets.at(index.row());

    QVariant value;
    switch (index.column()) {
    case SocketType:
        switch (socket->type()) {
        case SocketType::Signal:
            value = QVariant::fromValue(tr("signal"));
            break;
        case SocketType::Slot:
            value = QVariant::fromValue(tr("slot"));
            break;
        }
        break;
    case SocketSignature:
        value = QVariant::fromValue(socket->signature());
        break;
    case SocketOutsideEdge:
        switch (socket->outsideEdge()) {
        case Qt::LeftEdge:
            value = QVariant::fromValue(tr("leftEdge"));
            break;
        case Qt::TopEdge:
            value = QVariant::fromValue(tr("topEdge"));
            break;
        case Qt::RightEdge:
            value = QVariant::fromValue(tr("rightEdge"));
            break;
        case Qt::BottomEdge:
            value = QVariant::fromValue(tr("bottomEdge"));
            break;
        }
        break;
    case SocketOutsideEdgeOffset:
        value = QVariant::fromValue(socket->outsideEdgeOffset());
        break;
    case SocketInsideXPos:
        value = QVariant::fromValue(socket->insideXPos());
        break;
    case SocketInsideYPos:
        value = QVariant::fromValue(socket->insideYPos());
        break;
    }

    return value;
}

bool SocketTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;

    QVector<Socket*> objectSockets = m_component->objectSockets();
    if (index.row() > objectSockets.count())
        return false;

    bool success = false;
    Socket* socket = objectSockets.at(index.row());
    switch(index.column()) {
    case SocketOutsideEdge:
    case SocketOutsideEdgeOffset:
        // TODO
        return false;
    case SocketInsideXPos: {
        int pos = value.toInt(&success);
        if (!success)
            return false;
        if (pos == socket->insideXPos())
            return false;

        socket->setInsideXPos(pos);
        success = true;
        break;
    }
    case SocketInsideYPos:
        int pos = value.toInt(&success);
        if (!success)
            return false;
        if (pos == socket->insideYPos())
            return false;

        socket->setInsideYPos(pos);
        success = true;
        break;
    }

    if (success) {
        emit dataChanged(index, index, QVector<int>() << role);
    }

    return success;
}

Qt::ItemFlags SocketTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    QVector<Socket*> objectSockets = m_component->objectSockets();
    if (index.row() >= objectSockets.count())
        return Qt::NoItemFlags;

    Qt::ItemFlags itemFlags = Qt::NoItemFlags;
    switch (index.column()) {
    case SocketType:
    case SocketSignature:
    case SocketOutsideEdge:
    case SocketOutsideEdgeOffset:
        itemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        break;
    case SocketInsideXPos:
    case SocketInsideYPos:
        itemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
        break;
    }

    return itemFlags;
}
