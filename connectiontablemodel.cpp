#include "connectiontablemodel.h"

#include "connection.h"
#include "socket.h"

int ConnectionTableModel::OptionsRole = Qt::UserRole + 1;

ConnectionTableModel::ConnectionTableModel(QObject* parent, Component* component)
    : QAbstractTableModel(parent)
    , m_component(component)
{
    Q_ASSERT(!m_component.isNull());
}

QVariant ConnectionTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    QVariant data;
    if (orientation != Qt::Vertical) {
        switch (static_cast<ConnectionTableModelIndex>(section)) {
        case ConnectionTableModelIndex::Sender:
            data = tr("Sender");
            break;
        case ConnectionTableModelIndex::Signal:
            data = tr("Signal");
            break;
        case ConnectionTableModelIndex::Receiver:
            data = tr("Receiver");
            break;
        case ConnectionTableModelIndex::Slot:
            data = tr("Slot");
            break;
        case ConnectionTableModelIndex::Valid:
            data = tr("Valid");
            break;
        case ConnectionTableModelIndex::StartXPos:
            data = tr("Start X position");
            break;
        case ConnectionTableModelIndex::StartYPos:
            data = tr("Start Y position");
            break;
        case ConnectionTableModelIndex::EndXPos:
            data = tr("End X position");
            break;
        case ConnectionTableModelIndex::EndYPos:
            data = tr("End Y position");
            break;
        }
    } else {
        if (section >= m_component->objectConnections().count())
            return QVariant();

        data = QVariant::fromValue(section);
    }

    return data;
}

int ConnectionTableModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    return m_component->objectConnections().count();
}

int ConnectionTableModel::columnCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    // Sender, Signal, Receiver, Slot, Valid, StartXPos, StartYPos, EndXPos, EndYPos
    return 9;
}

QVariant ConnectionTableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto objectConnections = m_component->objectConnections();
    if (index.row() >= objectConnections.count())
        return QVariant();

    QPointer<Connection> connection = objectConnections.at(index.row());

    QVariant value;
    if (role == Qt::DisplayRole) {
        switch (static_cast<ConnectionTableModelIndex>(index.column())) {
        case ConnectionTableModelIndex::Sender:
            if (connection->sender()) {
                value = QVariant::fromValue(connection->sender()->parent()->objectName());
            } else {
                value = QVariant::fromValue(tr("<sender>"));
            }
            break;
        case ConnectionTableModelIndex::Signal:
            if (!connection->sender().isNull()) {
                value = QVariant::fromValue(connection->sender()->signature());
            } else {
                value = QVariant::fromValue(tr("<signal>"));
            }
            break;
        case ConnectionTableModelIndex::Receiver:
            if (connection->receiver()) {
                value = QVariant::fromValue(connection->receiver()->parent()->objectName());
            } else {
                value = QVariant::fromValue(tr("<receiver>"));
            }
            break;
        case ConnectionTableModelIndex::Slot:
            if (!connection->receiver().isNull()) {
                value = QVariant::fromValue(connection->receiver()->signature());
            } else {
                value = QVariant::fromValue(tr("<slot>"));
            }
            break;
        case ConnectionTableModelIndex::Valid:
            value = QVariant::fromValue(connection->isValid());
            break;
        case ConnectionTableModelIndex::StartXPos:
            if (!connection->sender().isNull())
                value = connection->sender()->outsideXPos();
            break;
        case ConnectionTableModelIndex::StartYPos:
            if (!connection->sender().isNull())
                value = connection->sender()->outsideYPos();
            break;
        case ConnectionTableModelIndex::EndXPos:
            if (!connection->receiver().isNull())
                value = connection->receiver()->outsideXPos();
            break;
        case ConnectionTableModelIndex::EndYPos:
            if (!connection->receiver().isNull())
                value = connection->receiver()->outsideYPos();
        }
    } else if (role == OptionsRole) {
        QStringList options;

        switch (static_cast<ConnectionTableModelIndex>(index.column())) {
        case ConnectionTableModelIndex::Sender:
        case ConnectionTableModelIndex::Receiver: {
            // TODO: consider checking compatibility of sender's signal with receiver's slots
            QList<QPointer<Component>> children = m_component->childComponents();
            for (auto it : children) {
                options.append(it->objectName());
            }
        } break;
        case ConnectionTableModelIndex::Signal: {
            options.append(tr("<signal>"));

            Component* caller = qobject_cast<Component*>(connection->sender()->parent());
            if (!caller)
                break;

            QVector<QMetaMethod> signalMethods = caller->objectSignals();
            for (auto it : signalMethods) {
                options.append(it.methodSignature());
            }
        } break;
        case ConnectionTableModelIndex::Slot: {
            // TODO: list only slots that have matching signature with selected signal
            options.append(tr("<slot>"));

            Component* callee = qobject_cast<Component*>(connection->receiver());
            if (!callee)
                break;

            QVector<QMetaMethod> slotMethods = callee->objectSlots();
            for (auto it : slotMethods) {
                options.append(it.methodSignature());
            }
        } break;
        case ConnectionTableModelIndex::Valid:
        case ConnectionTableModelIndex::StartXPos:
        case ConnectionTableModelIndex::StartYPos:
        case ConnectionTableModelIndex::EndXPos:
        case ConnectionTableModelIndex::EndYPos:
            // Not editable
            break;
        }

        value = QVariant::fromValue(options);
    }

    return value;
}

Qt::ItemFlags ConnectionTableModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    // TODO: flags
    //    QVector<Connection*> objectConnections = m_component->objectConnections();
    //    if (index.row() >= objectConnections.count())
    //        return Qt::NoItemFlags;

    //    Connection* connection = objectConnections.at(index.row());
    Qt::ItemFlags itemFlags = Qt::NoItemFlags;
    switch (static_cast<ConnectionTableModelIndex>(index.column())) {
    case ConnectionTableModelIndex::Sender:
    case ConnectionTableModelIndex::Signal:
    case ConnectionTableModelIndex::Receiver:
    case ConnectionTableModelIndex::Slot:
    case ConnectionTableModelIndex::Valid:
    case ConnectionTableModelIndex::StartXPos:
    case ConnectionTableModelIndex::StartYPos:
    case ConnectionTableModelIndex::EndXPos:
    case ConnectionTableModelIndex::EndYPos:
        itemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        break;
    }

    return itemFlags;
}
