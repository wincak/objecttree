#include "component.h"

#include <QMetaProperty>
#include <QTimer>

// Debug
#include <QDebug>

// for instantiation
#include "connection.h"
#include "socket.h"

Component::Component(Component* parent)
    : QObject(parent)
    , m_xPos(0)
    , m_yPos(0)
    , m_width(200)
    , m_height(75)
{
    // Socket initialization has to be postponed till after subclass q_properties get initialized
    // TODO: find a better way to do this
    QTimer::singleShot(0, this, SLOT(initializeSockets()));

    // Forward private QObject signal
    connect(this, &QObject::objectNameChanged, this, &Component::objectNameChanged);
    connect(this, &QObject::objectNameChanged, this, &Component::pathChanged);
}

Component::~Component()
{
}

QString Component::className() const
{
    return QString::fromLocal8Bit(metaObject()->className());
}

QString Component::path() const
{
    QString path;

    auto parent = qobject_cast<Component*>(this->parent());
    if (parent)
        path = parent->path();

    path.append('/');
    path.append(objectName());

    while (path.startsWith("//"))
        path.remove(0, 1);

    return path;
}

QPointer<Component> Component::child(QString path) const
{
    QPointer<Component> ptr;

    // Remove slash(es) at the beginning
    while (path.startsWith('/'))
        path.remove(0, 1);

    // Remove trailing slash(es) - there should be none
    Q_ASSERT(!path.endsWith('/'));
    while (path.endsWith('/'))
        path.truncate(path.length() - 1);

    if (path.isEmpty())
        return ptr;

    int firstSlash = path.indexOf('/');
    if (firstSlash < 0) {
        // No slashes - path points to direct child
        for (auto child : m_childItems) {
            if (child.isNull())
                continue;

            if (child->objectName() == path) {
                ptr = child;
                break;
            }
        }
    } else {
        // Slash present - path points deeper
        QString directChildName = path.left(firstSlash);
        QString subPath = path.mid(firstSlash + 1);
        for (auto child : m_childItems) {
            if (child.isNull())
                continue;

            if (child->objectName() == directChildName) {
                ptr = child->child(subPath);
                break;
            }
        }
    }

    return ptr;
}

int Component::childCount() const
{
    return m_childItems.count();
}

bool Component::addChild(QPointer<Component> child)
{
    if (child.isNull())
        return false;

    QStringList existingNames;
    for (auto existingChild : m_childItems) {
        existingNames.append(existingChild->objectName());
    }

    if (child->objectName().isEmpty() || existingNames.contains(child->objectName())) {
        QString newName = tr("New component");

        int index = 1;
        while (existingNames.contains(newName)) {
            newName = tr("New component [%1]").arg(index);
        }

        child->setObjectName(newName);
    }

    child->setParent(this);

    m_childItems.append(child);

    emit childAdded(child->objectName());

    return true;
}

bool Component::removeChild(const QString& name)
{
    QPointer<Component> childPtr = child(name);
    if (childPtr.isNull())
        return false;

    bool success = m_childItems.removeOne(childPtr);
    if (!success)
        return false;

    childPtr->deleteLater();

    emit childRemoved(name);

    return true;
}

int Component::childNumber() const
{
    auto parent = qobject_cast<Component*>(this->parent());
    if (!parent)
        return 0;

    return parent->m_childItems.indexOf(const_cast<Component*>(this));
}

bool Component::createConnection(Socket *sender, Socket *receiver)
{
   if (!sender || !receiver)
       return false;

   // Both objects parent components have to be either this object or a direct child
   Component* caller = qobject_cast<Component*>(sender->parent());
   if (!caller)
       return false;
   Component* callee = qobject_cast<Component*>(receiver->parent());
   if (!callee)
       return false;

   Connection* connection = new Connection(this, sender, receiver);
   if (!connection) {
       qWarning("Connection failed");
       return false;
   }

   m_connections.append(connection);

   emit connectionsChanged();

   return true;
}

QPointer<Socket> Component::socket(const QString &signature) const
{
    if (signature.isEmpty())
        return QPointer<Socket>();

    Socket* socketPtr = nullptr;
    for (auto s : m_sockets) {
        if (s->signature() == signature) {
            socketPtr = s;
            break;
        }
    }

    return socketPtr;
}

QList<QPointer<Component>> Component::childComponents() const
{
    return m_childItems;
}

QVariant Component::objectProperty(const QString& propertyId)
{
    // objectName is not a path, using property of this
    if (!propertyId.contains('/'))
        return property(qPrintable(propertyId));

    // objectName is a path, we need to find the child and pass value recursively
    QStringList path = propertyId.split('/');
    QString childName = path.takeFirst();

    QPointer<Component> child;
    for (QPointer<Component> it : m_childItems) {
        if (it->objectName() == childName) {
            child = it;
            break;
        }
    }

    if (child.isNull())
        return QVariant();

    // Put the ID back together and pass to the child
    QString childPropertyId = path.join('/');

    return child->objectProperty(childPropertyId);
}

bool Component::setObjectProperty(const QString& propertyId, const QVariant& value)
{
    // objectName is not a path, setting property of this
    if (!propertyId.contains('/'))
        return setProperty(qPrintable(propertyId), value);

    // objectName is a path, we need to find the child and pass value recursively
    QStringList path = propertyId.split('/');
    QString childName = path.takeFirst();

    QPointer<Component> child;
    for (QPointer<Component> it : m_childItems) {
        if (it->objectName() == childName) {
            child = it;
            break;
        }
    }

    if (child.isNull())
        return false;

    // Put the ID back together and pass to the child
    QString childPropertyId = path.join('/');

    if (childPropertyId == "objectName") {
        // Renaming a child. It's neccessary for the name to be unique among siblings
        for (QPointer<Component> child : m_childItems) {
            if (child->objectName() == value.toString())
                return false;
        }
    }

    return child->setObjectProperty(childPropertyId, value);
}

QVector<QMetaProperty> Component::objectProperties() const
{
    QVector<QMetaProperty> properties;

    const QMetaObject* mo = this->metaObject();

    do {
        QVector<QMetaProperty> temp;

        for (int i = mo->propertyOffset(); i < mo->propertyCount(); ++i) {
            temp.append(mo->property(i));
        }

        temp.append(properties);
        properties = temp;

        // We don't want to superclass higher than to Component
        if (QString::fromLocal8Bit(mo->className()) == "Component")
            break;
    } while ((mo = mo->superClass()));

    return properties;
}

QVector<QMetaMethod> Component::objectSignals() const
{
    QVector<QMetaMethod> list;

    const QMetaObject* mo = this->metaObject();

    int methodCount = mo->methodCount();
    for (int methodIndex = 0; methodIndex < methodCount; methodIndex++) {
        QMetaMethod method = mo->method(methodIndex);

        if (method.methodType() != QMetaMethod::Signal)
            continue;

        // Unneeded signals
        // TODO: try excluding all Component class signals, use only subclass signals
        if (method.name() == "destroyed")
            continue;
        if (method.name() == "xPosChanged")
            continue;
        if (method.name() == "yPosChanged")
            continue;
        if (method.name() == "widthChanged")
            continue;
        if (method.name() == "heightChanged")
            continue;
        if (method.name() == "connectionsChanged")
            continue;
        // NOTE: objectNameChanged is duplicated: one is QObject private signal, second is
        // is Component signal. It might be worthwhile to filter privat/protected signals as well.
        if (method.name() == "objectNameChanged")
            continue;

        list.append(method);
    }

    return list;
}

QVector<QMetaMethod> Component::objectSlots() const
{
    QVector<QMetaMethod> list;

    const QMetaObject* mo = this->metaObject();

    int methodCount = mo->methodCount();
    for (int methodIndex = 0; methodIndex < methodCount; methodIndex++) {
        QMetaMethod method = mo->method(methodIndex);

        if (method.methodType() != QMetaMethod::Slot)
            continue;

        // Unneeded slots
        // TODO: try excluding all Component class slots, use only subclass slots
        if (method.name() == "deleteLater")
            continue;
        if (method.name() == "_q_reregisterTimers")
            continue;
        if (method.name() == "initializeSockets")
            continue;

        list.append(method);
    }

    return list;
}

QVector<QPointer<Connection>> Component::objectConnections() const
{
    return m_connections;
}

QVector<Socket*> Component::objectSockets() const
{
    return m_sockets;
}

QVariantMap Component::serialize() const
{
    // Note: QHash entries are "arbitrarily sorted".

    QVariantMap map;
    const QMetaObject* mo = this->metaObject();

    for (int i = 0; i < mo->propertyCount(); ++i) {
        QMetaProperty prop = mo->property(i);

        if (!prop.isStored())
            continue;

        map.insert(prop.name(), QVariant::fromValue(prop.read(this)));
    }

    // TODO: is it better to use name-adressed hash or non-addressed list?
    // name-addressed hash guarantees unique names, but duplicates information
    // non-addressed list is simpler, but duplicate names can occur
    QVariantMap children;
    for (Component* child : this->childComponents()) {
        children.insert(child->objectName(), child->serialize());
    }
    if (!children.isEmpty())
        map.insert("children", QVariant::fromValue(children));

    // TODO: is it ok to have unordered connection list? How are they going to be addressed?
    // Will the connection order survive serialization and deserialization? Does it matter?
    QVariantList connections;
    for (QPointer<Connection> connection : this->objectConnections()) {
        QVariantMap details;
        QString sender;
        if (connection->sender())
            sender = connection->sender()->objectName();
        details.insert("sender", sender);
        QString signal;
        if (!connection->sender().isNull())
            signal = connection->sender()->signature();
        details.insert("signal", signal);
        QString receiver;
        if (connection->receiver())
            receiver = connection->receiver()->objectName();
        details.insert("receiver", receiver);
        QString slot;
        if (!connection->receiver().isNull())
            slot = connection->receiver()->signature();
        details.insert("slot", slot);

        connections.append(details);
    }
    if (!connections.isEmpty())
        map.insert("connections", QVariant::fromValue(connections));

    QVariantList sockets;
    for (Socket* socket : this->objectSockets()) {
        QVariantMap details;
        QString type;
        switch (socket->type()) {
        case SocketType::Signal:
            type = "signal";
            break;
        case SocketType::Slot:
            type = "slot";
            break;
        }
        details.insert("type", type);

        details.insert("signature", socket->signature());

        details.insert("outideEdge", socket->outsideEdge());
        details.insert("insideXPos", socket->insideXPos());
        details.insert("insideYPos", socket->insideYPos());

        sockets.append(details);
    }
    if (!sockets.isEmpty())
        map.insert("sockets", QVariant::fromValue(sockets));

    return map;
}

int Component::xPos() const
{
    return m_xPos;
}

void Component::setXPos(int xPos)
{
    if (xPos == m_xPos)
        return;

    m_xPos = xPos;

    emit xPosChanged(xPos);
}

int Component::yPos() const
{
    return m_yPos;
}

void Component::setYPos(int yPos)
{
    if (yPos == m_yPos)
        return;

    m_yPos = yPos;

    emit yPosChanged(yPos);
}

int Component::width() const
{
    return m_width;
}

void Component::setWidth(int width)
{
    if (width == m_width)
        return;

    m_width = width;

    emit widthChanged(width);
}

int Component::height() const
{
    return m_height;
}

void Component::setHeight(int height)
{
    if (height == m_height)
        return;

    m_height = height;

    emit heightChanged(height);
}

void Component::initializeSockets()
{
    QVector<QMetaMethod> objectSignals = this->objectSignals();
    for (int index = 0; index < objectSignals.count(); index++) {
        QMetaMethod signal = objectSignals.at(index);
        int gapSize = m_height / (objectSignals.count() + 1);
        int outsideEdgeOffset = gapSize * (index + 1);

        m_sockets.append(new Socket(this, signal, Qt::RightEdge, outsideEdgeOffset));
    }
    QVector<QMetaMethod> objectSlots = this->objectSlots();
    for (int index = 0; index < objectSlots.count(); index++) {
        QMetaMethod slot = objectSlots.at(index);
        int gapSize = m_height / (objectSlots.count() + 1);
        int outsideEdgeOffset = gapSize * (index + 1);

        m_sockets.append(new Socket(this, slot, Qt::LeftEdge, outsideEdgeOffset));
    }
}
