#include "console.h"

Console* Console::m_instance = nullptr;

Console::Console(QObject* parent)
    : QObject(parent)
    , m_messageLimit(1000)
{
}

Console* Console::instance()
{
    if (!m_instance)
        m_instance = new Console(nullptr);

    return m_instance;
}

QStringList Console::messages() const
{
    return m_messages;
}

int Console::messageLimit() const
{
    return m_messageLimit;
}

void Console::addMessage(const QString& message)
{
    if (message.isEmpty())
        return;

    if (m_messages.count() >= m_messageLimit)
        m_messages.removeFirst();

    m_messages.append(message);

    emit messagesChanged(m_messages);
}

void Console::setMessageLimit(int messageLimit)
{
    if (m_messageLimit == messageLimit)
        return;

    m_messageLimit = messageLimit;
    emit messageLimitChanged(m_messageLimit);
}
